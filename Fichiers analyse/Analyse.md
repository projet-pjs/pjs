# Résumé de la discussion du 26 avril 2024



## Technologies:

Nous allons garder les mêmes technologies utilisées pour le prototype
- Backend: ASP .NET CORE
- Frontend: Angular
- BD: présentement SQL-Server. Probablement la changer pour MySQL. Ca ne devrait pas causer de problèmes.
- Styles: Bootstrap, PrimeNg
- Le projet sera transféré sur ce GitLab. 
- Le site sera hébergé sur un serveur central pour toutes les écoles. 

## Fonctionnement:

- Un super admin crée le compte pour les profs. 
- Un prof crée ses questions.
- Création d'un éditeur de questions.
- Les questions sont dans des blocs de questions.
- Les questions sont en séquence dans le bloc.
- Il est possible d'entrer une réponse qui sera "asserted".
- Le prof met disponible la série d'exercices.
- Les étudiants se "connectent" avec seulement un nom (un identifiant unique lui sera créé).
- Est-ce que ca prend un compte, ou juste inscrire son nom
- Avoir un compte permet d'avoir un historique... est-ce overkill ?
- Une fois connecté, il inscrit le code de la série d'exercice.
- Est-ce dans l'url, ou une question qui est présenté après le "login" ?
- La série de bloc de questions est affichée
- L'étudiant choisie une série
- La séquence de questions est affichée
- L'étudiant choisie une question  
- Les questions sont "facultatives". 
- Est-ce qu'on peut partir de n'importe ou ? ou faut les faire dans l'ordre ?
- L'étudiant écrit le code et go.
- Certaines question auront une réponse qui peut être "asserted"
- Si c'est le cas, l'étudiant a un feedback automatique
- Il peut continuer a la prochaine question qu'il ait ou non la bonne réponse
- Le prof recoit une notification que l'étudiant a soumis une réponse et peut la vérifier
- Il n'a pas a corriger, mais peut aller voir l'étudiant pour lui faire des commentaires.
- Il peut cocher ok ou non pour donner un feedback sur les questions qui sont pas "asserted"
- Il faudra modifier la page d'aide afin qu'elle soit disponible sans sortir de l'exercice.
- Tab, ou popup?



