﻿using FluentMigrator;

namespace PJS.Data.Migrations;

[Migration(202405241626)]
public class AjouterColonneEstActiveClasses : Migration
{
    public override void Up()
    {
        Create.Column("est_active")
            .OnTable("classes")
            .AsBoolean();
    }

    public override void Down()
    {
        Delete.Column("est_active")
            .FromTable("classes");
    }
}