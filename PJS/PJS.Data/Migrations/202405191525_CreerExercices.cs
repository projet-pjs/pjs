﻿using FluentMigrator;

namespace PJS.Data.Migrations;

[Migration(202405191525)]
public class CreerExercices : Migration
{
    public override void Up()
    {
        Create.Table("exercices")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("nom").AsString()
            .WithColumn("description").AsString().Nullable()
            .WithColumn("index_sequence").AsInt32()
            .WithColumn("resultat_attendu_compilation").AsString().Nullable()
            .WithColumn("demarche_attendue").AsString().Nullable()
            .WithColumn("date_echeance").AsDateTime().Nullable();
        
    }

    public override void Down()
    {
        Delete.Table("exercices");
    }
}