﻿using FluentMigrator;
using FluentMigrator.SqlServer;

namespace PJS.Data.Migrations;

[Migration(202405161305)]
public class CreerEtudiants : Migration
{
    public override void Up()
    {
        Create.Table("etudiants")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("nom").AsString();
        
    }

    public override void Down()
    {
        Delete.Table("etudiants");
    }
}