﻿using FluentMigrator;
using FluentMigrator.SqlServer;

namespace PJS.Data.Migrations;

[Migration(202405161300)]
public class CreerEnseignants : Migration
{
    public override void Up()
    {
        Create.Table("enseignants")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("prenom").AsString()
            .WithColumn("nom").AsString()
            .WithColumn("adresse_courriel").AsString().Unique()
            .WithColumn("mot_de_passe").AsString();
        
    }

    public override void Down()
    {
        Delete.Table("enseignants");
    }
}