﻿using FluentMigrator;
using FluentMigrator.SqlServer;

namespace PJS.Data.Migrations;

[Migration(202405252215)]
public class AjouterSeedsBD : Migration
{
    public override void Up()
    {
        Insert.IntoTable("enseignants")
            .WithIdentityInsert()
            .Row(new { prenom = "Jia", nom = "Anu", adresse_courriel = "jiaanu@gmail.com", mot_de_passe = "12345" })
            .Row(new { prenom = "Naina", nom = "Anu", adresse_courriel = "nainaanu@gmail.com", mot_de_passe = "12345"})
            .Row(new { prenom = "Sreena", nom = "Anu", adresse_courriel = "sreenaanu@gmail.com", mot_de_passe = "12345"})
            .Row(new { prenom = "Fred", nom = "Viswan", adresse_courriel = "fredviswan@gmail.com", mot_de_passe = "12345"})
            .Row(new { prenom = "Benoit", nom = "Desrosiers", adresse_courriel = "BenoitDesrosiers@gmail.com", mot_de_passe = "12345"});
        
        Insert.IntoTable("etudiants")
            .WithIdentityInsert()
            .Row(new { nom = "Fabrice" })
            .Row(new { nom = "Ludovic" })
            .Row(new { nom = "Léo" })
            .Row(new { nom = "Mathilde" })
            .Row(new { nom = "Marie" });
        
        Insert.IntoTable("classes")
            .WithIdentityInsert()
            .Row(new
            {
                enseignant_id = 1, nom = "Introduction à la programmation",
                description = "Une classe pour apprendre les bases de la programmation",
                code_acces = "aeh123n",
                est_active = false
            })
            .Row(new
            {
                enseignant_id = 1, nom = "Introduction au boucles",
                description = "Une classe pour apprendre comment coder des boucles",
                code_acces = "jaiwhn1",
                est_active = false
            })
            .Row(new
            {
                enseignant_id = 1, nom = "Programmation web",
                description = "Une classe pour apprendre comment coder un site web",
                code_acces = "qh182nd",
                est_active = false
            })
            .Row(new
            {
                enseignant_id = 2, nom = "Introduction aux conditions",
                description = "Une classe pour apprendre comment utiliser les conditions if en programmation",
                code_acces = "paoshn27",
                est_active = false
            })
            .Row(new
            {
                enseignant_id = 3, nom = "Apprentissage des types de variables",
                description = "Une classe pour apprendre les différents types de variables",
                code_acces = "123adtf",
                est_active = false
            });
        
        Insert.IntoTable("exercices")
            .WithIdentityInsert()
            .Row(new
            {
                nom = "Exercice de boucle 1",
                description = "Un exercice sur les boucles",
                index_sequence = 1,
                resultat_attendu_compilation = "for (int i = 0, i < 10, i++)",
                demarche_attendue = "for (int i = 0, i < 10, i++)",
                date_echeance = "'2025-08-27'"
            })
            .Row(new
            {
                nom = "Exercice de boucle 2",
                description = "Un exercice sur les boucles",
                index_sequence = 2,
                resultat_attendu_compilation = "for (int i = 10, i > 0, i--)",
                demarche_attendue = "for (int i = 10, i > 0, i--)",
                date_echeance = "'2025-08-27'"
            })
            .Row(new
            {
                nom = "Exercice de variables",
                index_sequence = 1
            })
            .Row(new
            {
                nom = "Exercice de condition 1",
                description = "Un exercice sur les conditions",
                index_sequence = 1,
                date_echeance = "'2025-08-27'"
            })
            .Row(new
            {
                nom = "Exercice de condition 2",
                description = "Un exercice sur les conditions",
                index_sequence = 2,
                date_echeance = "'2025-08-27'"
            });
        
                Insert.IntoTable("reponses")
            .Row(new
            {
                etudiant_id = 1,
                exercice_id = 1,
                code_pjs = "Pour 0 à 10",
                resultat_console = "for (int i = 0, i < 10, i++)",
                est_consulte_par_etudiant = true,
                est_consulte_par_enseignant = false,
                est_reussi = true,
                commentaire = "Bravo !!!"
            })
            .Row(new
            {
                etudiant_id = 1,
                exercice_id = 2,
                code_pjs = "Pour 10 à 0",
                resultat_console = "for (int i = 10, i > 0, i--)",
                est_consulte_par_etudiant = true,
                est_consulte_par_enseignant = false,
                est_reussi = true,
                commentaire = "Tu as bien compris !"
            })
            .Row(new
            {
                etudiant_id = 2,
                exercice_id = 1,
                code_pjs = "blablabla",
                resultat_console = "for (int i = 0, i < 10, i++)",
                est_consulte_par_etudiant = false,
                est_consulte_par_enseignant = true,
                est_reussi = false,
                commentaire = "Franchement..."
            })
            .Row(new
            {
                etudiant_id = 2,
                exercice_id = 2,
                code_pjs = "Si 1 plus petit que 2",
                resultat_console = "for (int i = 0, i < 10, i++)",
                est_consulte_par_etudiant = false,
                est_consulte_par_enseignant = true,
                est_reussi = false
            })
            .Row(new
            {
                etudiant_id = 3,
                exercice_id = 3,
                code_pjs = "Pour 0 à 10",
                resultat_console = "for (int i = 0, i < 10, i++)",
                est_consulte_par_etudiant = false,
                est_consulte_par_enseignant = true,
                est_reussi = true
            });
                
        Insert.IntoTable("etudiants_classes")
            .Row(new { etudiant_id = 1, classe_id = 1 })
            .Row(new { etudiant_id = 1, classe_id = 2 })
            .Row(new { etudiant_id = 2, classe_id = 2 })
            .Row(new { etudiant_id = 3, classe_id = 3 })
            .Row(new { etudiant_id = 4, classe_id = 4 });
        
        Insert.IntoTable("classes_exercices")
            .Row(new { exercice_id = 1, classe_id = 1 })
            .Row(new { exercice_id = 1, classe_id = 2 })
            .Row(new { exercice_id = 2, classe_id = 2 })
            .Row(new { exercice_id = 3, classe_id = 3 })
            .Row(new { exercice_id = 4, classe_id = 4 });
        
        Insert.IntoTable("admins")
            .Row(new { code = "7d66f9c1-910e-4f4f-9210-afb9db78957c" })
            .Row(new { code = "b30d80b6-a669-4e6e-95c4-c34651e5ad7e" })
            .Row(new { code = "ca378d6f-5b91-4036-af46-686eb7b9a29b" })
            .Row(new { code = "8f63db70-913d-4936-aad1-ddf328285444" })
            .Row(new { code = "a3ecb189-e076-433f-be88-4f1809dc3731" });

    }

    public override void Down()
    {
        Delete.FromTable("admins")
            .Row(new { code = "7d66f9c1-910e-4f4f-9210-afb9db78957c" })
            .Row(new { code = "b30d80b6-a669-4e6e-95c4-c34651e5ad7e" })
            .Row(new { code = "ca378d6f-5b91-4036-af46-686eb7b9a29b" })
            .Row(new { code = "8f63db70-913d-4936-aad1-ddf328285444" })
            .Row(new { code = "a3ecb189-e076-433f-be88-4f1809dc3731" });
        
        Delete.FromTable("classes_exercices")
            .Row(new { exercice_id = 1, classe_id = 1 })
            .Row(new { exercice_id = 1, classe_id = 2 })
            .Row(new { exercice_id = 2, classe_id = 2 })
            .Row(new { exercice_id = 3, classe_id = 3 })
            .Row(new { exercice_id = 4, classe_id = 4 });
        
        Delete.FromTable("etudiants_classes")
            .Row(new { etudiant_id = 1, classe_id = 1 })
            .Row(new { etudiant_id = 1, classe_id = 2 })
            .Row(new { etudiant_id = 2, classe_id = 2 })
            .Row(new { etudiant_id = 3, classe_id = 3 })
            .Row(new { etudiant_id = 4, classe_id = 4 });
        
        Delete.FromTable("reponses")
            .Row(new
            {
                etudiant_id = 1,
                exercice_id = 1,
                code_pjs = "Pour 0 à 10",
                resultat_console = "for (int i = 0, i < 10, i++)",
            })
            .Row(new
            {
                etudiant_id = 1,
                exercice_id = 2,
                code_pjs = "Pour 10 à 0",
                resultat_console = "for (int i = 10, i > 0, i--)",
            })
            .Row(new
            {
                etudiant_id = 2,
                exercice_id = 1,
                code_pjs = "blablabla",
                resultat_console = "for (int i = 0, i < 10, i++)",
            })
            .Row(new
            {
                etudiant_id = 2,
                exercice_id = 2,
                code_pjs = "Si 1 plus petit que 2",
                resultat_console = "for (int i = 0, i < 10, i++)",
            })
            .Row(new
            {
                etudiant_id = 3,
                exercice_id = 3,
                code_pjs = "Pour 0 à 10",
                resultat_console = "for (int i = 0, i < 10, i++)",
            });
        
        Delete.FromTable("exercices")
            .Row(new
            {
                nom = "Exercice de boucle 1",
                description = "Un exercice sur les boucles",
                index_sequence = 1,
                resultat_attendu_compilation = "for (int i = 0, i < 10, i++)",
                demarche_attendue = "for (int i = 0, i < 10, i++)",
            })
            .Row(new
            {
                nom = "Exercice de boucle 2",
                description = "Un exercice sur les boucles",
                index_sequence = 2,
                resultat_attendu_compilation = "for (int i = 10, i > 0, i--)",
                demarche_attendue = "for (int i = 10, i > 0, i--)",
            })
            .Row(new
            {
                nom = "Exercice de variables",
                index_sequence = 1
            })
            .Row(new
            {
                nom = "Exercice de condition 1",
                description = "Un exercice sur les conditions",
                index_sequence = 1,
            })
            .Row(new
            {
                nom = "Exercice de condition 2",
                description = "Un exercice sur les conditions",
                index_sequence = 2,
            });
        
        Delete.FromTable("classes")
            .Row(new
            {
                enseignant_id = 1, nom = "Introduction à la programmation",
                description = "Une classe pour apprendre les bases de la programmation",
                code_acces = "aeh123n",
            })
            .Row(new
            {
                enseignant_id = 1, nom = "Introduction au boucles",
                description = "Une classe pour apprendre comment coder des boucles",
                code_acces = "jaiwhn1"
            })
            .Row(new
            {
                enseignant_id = 1, nom = "Programmation web",
                description = "Une classe pour apprendre comment coder un site web",
                code_acces = "qh182nd"
            })
            .Row(new
            {
                enseignant_id = 2, nom = "Introduction aux conditions",
                description = "Une classe pour apprendre comment utiliser les conditions if en programmation",
                code_acces = "paoshn27"
            })
            .Row(new
            {
                enseignant_id = 3, nom = "Apprentissage des types de variables",
                description = "Une classe pour apprendre les différents types de variables",
                code_acces = "123adtf"
            });
        
        Delete.FromTable("etudiants")
            .Row(new { nom = "Fabrice" })
            .Row(new { nom = "Ludovic" })
            .Row(new { nom = "Léo" })
            .Row(new { nom = "Mathilde" })
            .Row(new { nom = "Marie" });
        
        Delete.FromTable("enseignants")
            .Row(new { prenom = "Jia", nom = "Anu", adresse_courriel = "jiaanu@gmail.com", mot_de_passe = "12345" })
            .Row(new { prenom = "Naina", nom = "Anu", adresse_courriel = "nainaanu@gmail.com", mot_de_passe = "12345"})
            .Row(new { prenom = "Sreena", nom = "Anu", adresse_courriel = "sreenaanu@gmail.com", mot_de_passe = "12345"})
            .Row(new { prenom = "Fred", nom = "Viswan", adresse_courriel = "fredviswan@gmail.com", mot_de_passe = "12345"})
            .Row(new { prenom = "Benoit", nom = "Desrosiers", adresse_courriel = "BenoitDesrosiers@gmail.com", mot_de_passe = "12345"});
    }
}