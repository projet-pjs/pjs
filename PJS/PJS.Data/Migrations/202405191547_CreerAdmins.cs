﻿using FluentMigrator;
using FluentMigrator.SqlServer;

namespace PJS.Data.Migrations;

[Migration(202405191547)]
public class CreerAdmins : Migration
{
    public override void Up()
    {
        Create.Table("admins")
            .WithColumn("code").AsString(255).PrimaryKey().WithDefault(SystemMethods.NewGuid);
        
    }

    public override void Down()
    {
        Delete.Table("admins");
    }
}