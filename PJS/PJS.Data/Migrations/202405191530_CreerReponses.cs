﻿using FluentMigrator;

namespace PJS.Data.Migrations;

[Migration(202405191530)]
public class CreerReponses : Migration
{
    public override void Up()
    {
        Create.Table("reponses")
            .WithColumn("etudiant_id").AsInt32().PrimaryKey()
            .WithColumn("exercice_id").AsInt32().PrimaryKey()
            .WithColumn("code_pjs").AsString()
            .WithColumn("resultat_console").AsString()
            .WithColumn("est_consulte_par_etudiant").AsBoolean().WithDefaultValue(true)
            .WithColumn("est_consulte_par_enseignant").AsBoolean().WithDefaultValue(false)
            .WithColumn("est_reussi").AsBoolean().WithDefaultValue(false)
            .WithColumn("commentaire").AsString().Nullable();
        
        Create.ForeignKey().FromTable("reponses").ForeignColumn("etudiant_id")
            .ToTable("etudiants").PrimaryColumn("id");

        Create.ForeignKey().FromTable("reponses").ForeignColumn("exercice_id")
            .ToTable("exercices").PrimaryColumn("id");

    }

    public override void Down()
    {
        Delete.ForeignKey().FromTable("reponses").ForeignColumn("etudiant_id")
            .ToTable("etudiants").PrimaryColumn("id");

        Delete.ForeignKey().FromTable("reponses").ForeignColumn("exercice_id")
            .ToTable("exercices").PrimaryColumn("id");

        Delete.Table("reponses");
    }
}