﻿using FluentMigrator;
using FluentMigrator.SqlServer;

namespace PJS.Data.Migrations;

[Migration(202405191252)]
public class CreerClasses : Migration
{
    public override void Up()
    {
        Create.Table("classes")
            .WithColumn("id").AsInt32().PrimaryKey().Identity()
            .WithColumn("enseignant_id").AsInt32()
            .WithColumn("nom").AsString()
            .WithColumn("description").AsString()
            .WithColumn("code_acces").AsString().Unique();
        
        Create.ForeignKey()
            .FromTable("classes").ForeignColumn("enseignant_id")
            .ToTable("enseignants").PrimaryColumn("id");
        
        Create.Index("index_classes_utilisateur_id")
            .OnTable("classes")
            .OnColumn("enseignant_id");
        
    }

    public override void Down()
    {
        Delete.ForeignKey()
            .FromTable("classes").ForeignColumn("enseignant_id")
            .ToTable("enseignants").PrimaryColumn("id");
        
        Delete.Table("classes");
    }
}