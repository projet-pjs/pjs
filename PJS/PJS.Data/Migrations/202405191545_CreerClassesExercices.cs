﻿using FluentMigrator;

namespace PJS.Data.Migrations;

[Migration(202405191545)]
public class CreerClassesExercices : Migration
{
    public override void Up()
    {
        Create.Table("classes_exercices")
            .WithColumn("classe_id").AsInt32().PrimaryKey()
            .WithColumn("exercice_id").AsInt32().PrimaryKey();
        
        Create.ForeignKey().FromTable("classes_exercices").ForeignColumn("classe_id")
            .ToTable("classes").PrimaryColumn("id");

        Create.ForeignKey().FromTable("classes_exercices").ForeignColumn("exercice_id")
            .ToTable("exercices").PrimaryColumn("id");
        
    }

    public override void Down()
    {
        Delete.ForeignKey().FromTable("classes_exercices").ForeignColumn("classe_id")
            .ToTable("classes").PrimaryColumn("id");

        Delete.ForeignKey().FromTable("classes_exercices").ForeignColumn("exercice_id")
            .ToTable("exercices").PrimaryColumn("id");

        Delete.Table("reponses");
    }
}