﻿using FluentMigrator;

namespace PJS.Data.Migrations;

[Migration(202405191542)]
public class CreerEtudiantsClasses : Migration
{
    public override void Up()
    {
        Create.Table("etudiants_classes")
            .WithColumn("etudiant_id").AsInt32().PrimaryKey()
            .WithColumn("classe_id").AsInt32().PrimaryKey();
        
        Create.ForeignKey().FromTable("etudiants_classes").ForeignColumn("etudiant_id")
            .ToTable("etudiants").PrimaryColumn("id");

        Create.ForeignKey().FromTable("etudiants_classes").ForeignColumn("classe_id")
            .ToTable("classes").PrimaryColumn("id");
        
    }

    public override void Down()
    {
        Delete.ForeignKey().FromTable("etudiants_classes").ForeignColumn("etudiant_id")
            .ToTable("etudiants").PrimaryColumn("id");

        Delete.ForeignKey().FromTable("etudiants_classes").ForeignColumn("classe_id")
            .ToTable("classes").PrimaryColumn("id");

        Delete.Table("reponses");
    }
}