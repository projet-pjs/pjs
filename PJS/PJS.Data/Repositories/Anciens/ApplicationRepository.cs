﻿using Dapper;
using PJS.Data.Context;
using PJS.Domain.Models.AnciensModels;
using System.Data;

namespace PJS.Data.Repositories.Anciens;

public class ApplicationRepository : IApplicationRepository
{

    private readonly DapperContext _db;

    public ApplicationRepository(DapperContext dbContexte)
    {
        _db = dbContexte;
    }


    public async Task<List<Matiere>> GetAllMatieres()
    {
        var sql = @"
            SELECT 
                *
            FROM
                Matiere m
            ORDER BY    
                m.Nom;
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Matiere> matieres = await connection.QueryAsync<Matiere>(sql);

            return matieres.ToList();
        }
    }

    
    public async Task<List<Tache>> GetAllTaches()
    {
        var sql = @"
            SELECT 
                *
            FROM
                Tache t
            INNER JOIN
                Matiere m on t.MatiereId = m.Id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Tache> taches = await connection.QueryAsync<Tache, Matiere, Tache>
            (sql, (tache, matiere) =>
            {
                tache.Matiere = matiere;
                return tache;
            });

            return taches.ToList();
        }
    }


    public async Task<List<Tache>> GetTachesByMatiereId(int id)
    {
        var sql = @"
            SELECT 
                *
            FROM
                Tache t
            INNER JOIN
                Matiere m on t.MatiereId = m.Id
            WHERE
                MatiereId = @MatiereId
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var param = new
            {
                MatiereId = id
            };

            IEnumerable<Tache> taches = await connection.QueryAsync<Tache, Matiere, Tache>
            (sql, (tache, matiere) =>
            {
                tache.Matiere = matiere;
                return tache;
            }, param);

            return taches.ToList();
        }
    }


    public async Task<Tache?> GetTacheById(int id)
    {
        var sql = @"
            SELECT 
                *
            FROM
                Tache t
            INNER JOIN
                Matiere m on t.MatiereId = m.Id
            WHERE
                t.Id = @Id;
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var param = new
            {
                Id = id
            };

            IEnumerable<Tache> taches = await connection.QueryAsync<Tache, Matiere, Tache>
                (sql, (tache, matiere) =>
                {
                    tache.Matiere = matiere;
                    return tache;
                }, param);

            return taches.FirstOrDefault();
        }
    }


    public async Task<Matiere?> GetMatiereById(int id)
    {
        var sql = @"
            SELECT 
                *
            FROM
                Matiere m
            WHERE
                Id = @Id;
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var param = new
            {
                Id = id
            };

            IEnumerable<Matiere> matieres = await connection.QueryAsync<Matiere>(sql, param);

            if (matieres == null)
            {
                matieres = new List<Matiere>();
            }

            return matieres.FirstOrDefault();
        }
    }
    
}
