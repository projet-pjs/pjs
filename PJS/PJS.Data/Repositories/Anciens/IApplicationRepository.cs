﻿using PJS.Domain.Models.AnciensModels;

namespace PJS.Data.Repositories.Anciens;

public interface IApplicationRepository
{
    #region GET

    Task<List<Matiere>> GetAllMatieres();

    Task<List<Tache>> GetAllTaches();

    Task<List<Tache>> GetTachesByMatiereId(int id);

    Task<Tache?> GetTacheById(int id);

    Task<Matiere?> GetMatiereById(int id);
    

    #endregion
    
}
