﻿using PJS.Domain.Models;

namespace PJS.Data.Repositories.Interfaces;

public interface IReponseRepository
{
    Task<List<Reponse>> GetReponsesPourExercices(List<int> exerciceIds, int etudiantId);

}
