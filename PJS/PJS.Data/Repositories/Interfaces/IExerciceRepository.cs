﻿
using PJS.Domain.Models;

namespace PJS.Data.Repositories.Interfaces;

public interface IExerciceRepository
{

    Task<List<Exercice>> FindByClasseId(int classeId);

    Task<List<Exercice>> GetAllExercices();
    
    Task<Exercice> Create(Exercice exercice);
}
