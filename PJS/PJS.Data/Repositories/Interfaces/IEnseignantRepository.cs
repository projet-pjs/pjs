﻿
using PJS.Domain.Models;

namespace PJS.Data.Repositories.Interfaces;

public interface IEnseignantRepository
{
    Task<Enseignant> GetEnseignantParCourriel(string courriel);
    
    Task<Enseignant> CreerEnseignant(Enseignant enseignant);
}
