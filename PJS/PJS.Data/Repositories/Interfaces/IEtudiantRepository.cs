﻿
using PJS.Domain.Models;

namespace PJS.Data.Repositories.Interfaces;

public interface IEtudiantRepository
{
    Task<List<Etudiant>> GetAll();
    
    Task<Etudiant?> FindById(int id);

    Task<Etudiant?> EstDansClasseSelectionne(int id, int classeId);

    Task<List<Etudiant>> FindByClasseId(int classeId);
    
    Task<Etudiant> Create(Etudiant etudiant);

    Task<bool> Delete(int id); 
}
