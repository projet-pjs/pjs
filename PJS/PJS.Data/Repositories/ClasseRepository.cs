﻿using System.Data;
using Dapper;
using PJS.Data.Context;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Data.Repositories;

public class ClasseRepository : IClasseRepository
{

    private readonly DapperContext _db;

    public ClasseRepository(DapperContext contexte)
    {
        _db = contexte;
    }

    public async Task<List<Classe>> GetAll()
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                classes;
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Classe> classes = await connection.QueryAsync<Classe>(sql);
            return classes.ToList();
        }
    }

    public async Task<bool> VerifieClasseEstEncoreOuverte(int classeId)
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                classes
            WHERE 
                id = @Id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            Classe? classe = await connection.QuerySingleOrDefaultAsync<Classe>(sql,
                new
                {
                    Id = classeId
                }
            );
            return (classe != null) && (classe.EstActive == true);
        }
    }

    public async Task<List<Classe>> FindByEnseignantId(int enseignantId)
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                classes
            WHERE 
                enseignant_id = @EnseignantId
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Classe> classes = await connection.QueryAsync<Classe>(sql,
                new
                {
                    EnseignantId = enseignantId
                });
            return classes.ToList();
        }
    }

    public async Task<Classe?> FindById(int id)
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                classes
            WHERE 
                id = @Id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            Classe? classe = await connection.QuerySingleOrDefaultAsync<Classe>(sql,
                new
                {
                    Id = id
                });
            return classe;
        }
    }

    public async Task<Classe?> FindByCodeAcces(string codeAcces)
    {
        string sql = @"
            SELECT 
                * 
            FROM
                classes
            WHERE
                code_acces = @CodeAcces
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            Classe? classe = await connection.QuerySingleOrDefaultAsync<Classe>(sql,
                new
                {
                    CodeAcces = codeAcces
                });
            return classe;
        }
    }


    public async Task<Classe> Create(Classe classe)
    {
        string sql = @"
        INSERT INTO 
            classes (enseignant_id, nom, description, code_acces, est_active)
        VALUES
            (@EnseignantId, @Nom, @Description, @CodeAcces, @EstActive)
        RETURNING 
            id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            classe.Id = await connection.QuerySingleAsync<int>(sql, classe);
        }

        return classe;
    }

    public async Task<bool> Update(Classe classe)
    {
        string sql = @"
        UPDATE 
            classes 
        SET
            enseignant_id = @EnseignantId,
            nom = @Nom,
            description = @Description,
            code_acces = @CodeAcces,
            est_active = @EstActive
        WHERE 
            id = @Id";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var affectedRows = await connection.ExecuteAsync(sql, classe);
            return affectedRows == 1;
        }
    }

    public async Task<bool> Delete(int id)
    {
        string sql = @"
            DELETE FROM 
                classes
            WHERE 
                id = @Id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var affectedRows = await connection.ExecuteAsync(sql, new { Id = id });
            return affectedRows == 1;
        }
    }
}
