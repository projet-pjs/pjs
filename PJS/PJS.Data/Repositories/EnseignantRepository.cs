﻿using System.Data;
using Dapper;
using PJS.Data.Context;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Data.Repositories;

public class EnseignantRepository : IEnseignantRepository
{

    private readonly DapperContext _db;

    public EnseignantRepository(DapperContext contexte)
    {
        _db = contexte;
    }

    public async Task<Enseignant> GetEnseignantParCourriel(string courriel)
    {
        string sql = @"
        SELECT * from enseignants
        WHERE adresse_courriel = @Courriel
    ";
        
        using (IDbConnection connection = _db.CreateConnection())
        {
            Enseignant? enseignant = await connection.QuerySingleOrDefaultAsync<Enseignant>(sql, 
                new
                {
                    Courriel = courriel
                });
            return enseignant;
        }
    }

    public async Task<Enseignant> CreerEnseignant(Enseignant enseignant)
    {
        string sql = @"
        INSERT INTO enseignants (prenom, nom, adresse_courriel, mot_de_passe)
        VALUES(@Prenom, @Nom, @AdresseCourriel, @MotDePasse)
        RETURNING id";
        
        string mdpHash = BCrypt.Net.BCrypt.HashPassword(enseignant.MotDePasse);
        
        enseignant.MotDePasse = mdpHash;
        
        using (IDbConnection connection = _db.CreateConnection())
        { 
            enseignant.Id = await connection.QuerySingleAsync<int>(sql, enseignant);
        }
        
        return enseignant;
    }
}
