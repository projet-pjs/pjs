﻿using Dapper;
using PJS.Data.Context;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;
using System.Data;

namespace PJS.Data.Repositories;

public class ReponseRepository : IReponseRepository
{

    private readonly DapperContext _db;

    public ReponseRepository(DapperContext contexte)
    {
        _db = contexte;
    }

    public async Task<List<Reponse>> GetReponsesPourExercices(List<int> exerciceIds, int etudiantId)
    {
        string sql = @"
            SELECT 
                r.* 
            FROM 
                reponses r
            WHERE
                r.etudiant_id = @EtudiantId AND
                r.exercice_id = ANY(@ExerciceIds::int[])
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var reponses = await connection.QueryAsync<Reponse>(sql,
                new
                {
                    EtudiantId = etudiantId,
                    ExerciceIds = exerciceIds
                });

            return reponses.ToList();
        }
    }

}
