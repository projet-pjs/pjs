using System.Data;
using Dapper;
using PJS.Data.Context;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;


namespace PJS.Data.Repositories;

public class ExerciceRepository : IExerciceRepository
{

    private readonly DapperContext _db;

    public ExerciceRepository(DapperContext contexte)
    {
        _db = contexte;
    }

    
    public async Task<List<Exercice>> FindByClasseId(int classeId)
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                exercices e
            INNER JOIN 
                classes_exercices ce ON e.id = ce.exercice_id
            WHERE 
                ce.classe_id = @ClasseId
            ORDER BY 
                index_sequence ASC;
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var exercices = await connection.QueryAsync<Exercice>(sql,
                new
                {
                    ClasseId = classeId
                });

            return exercices.ToList();
        }
    }


    public async Task<List<Exercice>> GetAllExercices()
    {
        var sql = @"
            SELECT 
                *
            FROM
                exercices e
            ORDER BY    
                e.index_sequence;
        ";
        
        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Exercice> exercices = await connection.QueryAsync<Exercice>(sql);

            return exercices.ToList();
        }
    }
        public async Task<Exercice> Create(Exercice exercice)
        {
            string sqlExercice = @"
            INSERT INTO exercices (nom, description, index_sequence, resultat_attendu_compilation, demarche_attendue, date_echeance)
            VALUES(@Nom, @Description, @IndexSequence, @ResultatAttenduCompilation, @DemarcheAttendue, @DateEcheance)
            RETURNING id";
            
            string sqlclasseExercice = @"
            INSERT INTO classes_exercices (classe_id, exercice_id)
            VALUES( @ClasseId, @ExerciceId)
            ";
            
            // Changer la date pour la date reçue dans la classe lorsqu'elle sera ajouté dans la partie client
            exercice.DateEcheance = DateTime.Now;
            
            using (IDbConnection connection = _db.CreateConnection())
            { 
                exercice.Id = await connection.QuerySingleAsync<int>(sqlExercice, exercice);
                await connection.QuerySingleAsync(sqlclasseExercice, new { ClasseId = exercice.ClasseId, ExerciceId = exercice.Id});
            }
            
            return exercice;
        }
}
