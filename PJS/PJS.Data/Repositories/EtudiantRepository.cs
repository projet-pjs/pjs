﻿using System.Data;
using Dapper;
using PJS.Data.Context;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Data.Repositories;

public class EtudiantRepository : IEtudiantRepository
{

    private readonly DapperContext _db;

    public EtudiantRepository(DapperContext contexte)
    {
        _db = contexte;
    }

    public async Task<List<Etudiant>> GetAll()
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                etudiants;
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Etudiant> etudiants = await connection.QueryAsync<Etudiant>(sql);
            return etudiants.ToList();
        }
    }

    public async Task<Etudiant?> FindById(int id)
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                etudiants
            WHERE 
                id = @Id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            Etudiant? etudiant = await connection.QuerySingleOrDefaultAsync<Etudiant>(sql,
                new
                {
                    Id = id
                });
            return etudiant;
        }
    }

    public async Task<Etudiant?> EstDansClasseSelectionne(int id, int classeId)
    {
        string sql = @"
            SELECT 
                e.* 
            FROM 
                etudiants e
            INNER JOIN 
                etudiants_classes ec ON e.id = ec.etudiant_id
            WHERE 
                e.id = @Id AND ec.classe_id = @ClasseId
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            Etudiant? etudiant = await connection.QuerySingleOrDefaultAsync<Etudiant>(sql,
                new
                {
                    Id = id,
                    ClasseId = classeId
                });
            return etudiant;
        }
    }

    public async Task<List<Etudiant>> FindByClasseId(int classeId)
    {
        string sql = @"
            SELECT 
                * 
            FROM 
                etudiants
            INNER JOIN 
                public.etudiants_classes ec ON etudiants.id = ec.etudiant_id
            WHERE 
                ec.classe_id = @ClasseId
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            IEnumerable<Etudiant> etudiants = await connection.QueryAsync<Etudiant>(sql,
                new
                {
                    ClasseId = classeId
                });
            return etudiants.ToList();
        }
    }

    public async Task<Etudiant> Create(Etudiant etudiant)
    {
        string sqlEtudiant = @"
            INSERT INTO 
                etudiants (nom)
            VALUES
                (@Nom)
            RETURNING id";

        string sqlEtudiantClasse = @"
            INSERT INTO 
                etudiants_classes (etudiant_id, classe_id)
            VALUES
                (@EtudiantId, @ClasseId)
        ";


        using (IDbConnection connection = _db.CreateConnection())
        {
            etudiant.Id = await connection.QuerySingleAsync<int>(sqlEtudiant, etudiant);
            var etudiantClasse = new { EtudiantId = etudiant.Id, ClasseId = etudiant.ClasseId };
            await connection.ExecuteAsync(sqlEtudiantClasse, etudiantClasse);
        }
        return etudiant;
    }

    public async Task<bool> Delete(int id)
    {
        string sql = @"
            DELETE FROM etudiants
            WHERE id = @Id
        ";

        using (IDbConnection connection = _db.CreateConnection())
        {
            var affectedRows = await connection.ExecuteAsync(sql, new { Id = id });
            return affectedRows == 1;
        }
    }
}
