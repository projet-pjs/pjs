﻿using Microsoft.Extensions.Configuration;
using System.Data;
using System.Data.SqlClient;
using Npgsql;

namespace PJS.Data.Context;

/// <summary>
/// Contexte de la base de données
/// </summary>
public class DapperContext
{
    /// <summary>
    /// Chaine de connexion
    /// </summary>
    private readonly string _connection;

    /// <summary>
    /// Constructeur
    /// </summary>
    /// <param name="configuration"></param>
    public DapperContext(IConfiguration configuration) {
        _connection = configuration.GetConnectionString("AppDatabaseConnection")!;
    }

    /// <summary>
    /// S'occupe de créer la connexion
    /// </summary>
    /// <returns></returns>
    public IDbConnection CreateConnection()
        => new NpgsqlConnection(_connection);
}
