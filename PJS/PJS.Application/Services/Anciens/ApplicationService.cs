﻿using PJS.Data.Repositories.Anciens;
using PJS.Domain.Models.AnciensModels;

namespace PJS.Application.Services.Anciens;

public class ApplicationService : IApplicationService
{
    #region Propriétés de classe


    private IApplicationRepository _applicationRepository;


    #endregion

    #region Constructeur


    public ApplicationService(IApplicationRepository applicationRepository)
    {
        _applicationRepository = applicationRepository;
    }


    #endregion

    #region GET

    

    public async Task<List<Matiere>> GetAllMatieres()
    {
        return await _applicationRepository.GetAllMatieres();
    }

    public async Task<List<Tache>> GetAllTaches()
    {
        return await _applicationRepository.GetAllTaches();
    }

    public async Task<Matiere?> GetMatiereById(int id)
    {
        return await GetMatiereById(id);
    }

    public async Task<Tache?> GetTacheById(int id)
    {
        return await _applicationRepository.GetTacheById(id);
    }

    public async Task<List<Tache>> GetTachesByMatiereId(int id)
    {
        return await _applicationRepository.GetTachesByMatiereId(id);
    }

    #endregion
    
}
