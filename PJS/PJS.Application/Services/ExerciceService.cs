﻿using PJS.Application.Services.Interfaces;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Application.Services;

public class ExerciceService : IExerciceService
{

    private readonly IExerciceRepository _exerciceRepository;

    public ExerciceService(IExerciceRepository exerciceRepository)
    {
        _exerciceRepository = exerciceRepository;
    }
    
    public async Task<List<Exercice>> GetAllExercices()
    {
        return await _exerciceRepository.GetAllExercices();
    }

    public async Task<List<Exercice>> FindByClasseId(int classeId)
    {
        try
        {
            var results = await _exerciceRepository.FindByClasseId(classeId);

            return results;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }

    public async Task<Exercice> Create(Exercice exercice)
    {
        return await _exerciceRepository.Create(exercice);
    }
}
