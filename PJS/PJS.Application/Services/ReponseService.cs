﻿using PJS.Application.Services.Interfaces;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Application.Services;

public class ReponseService : IReponseService
{

    private readonly IReponseRepository _reponseRepository;

    public ReponseService(IReponseRepository reponseRepository)
    {
        _reponseRepository = reponseRepository;
    }

    public async Task<List<Reponse>> GetReponsesPourExercices(List<int> exerciceIds, int etudiantId)
    {
        try
        {
            var results = await _reponseRepository.GetReponsesPourExercices(exerciceIds, etudiantId);

            return results;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }
}
