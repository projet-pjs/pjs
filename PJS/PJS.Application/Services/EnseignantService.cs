﻿using PJS.Application.Services.Interfaces;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Application.Services;

public class EnseignantService : IEnseignantService
{

    private readonly IEnseignantRepository _enseignanteRepository;

    public EnseignantService(IEnseignantRepository enseignantRepository)
    {
        _enseignanteRepository = enseignantRepository;
    }

    public async Task<Enseignant?> GetEnseignantParCourrielMdp(string courriel, string motDePasse)
    {
        Enseignant enseignant = await _enseignanteRepository.GetEnseignantParCourriel(courriel);

        if (BCrypt.Net.BCrypt.Verify(motDePasse, enseignant.MotDePasse))
        {
            return enseignant;
        }
        
        return null;
        
    }

    public async Task<Enseignant> CreerEnseignant(Enseignant enseignant)
    {
        return await _enseignanteRepository.CreerEnseignant(enseignant);
    }
}
