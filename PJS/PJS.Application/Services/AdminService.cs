﻿using PJS.Application.Services.Interfaces;
using PJS.Data.Repositories.Interfaces;

namespace PJS.Application.Services;

public class AdminService : IAdminService
{

    private readonly IAdminRepository _adminRepository;

    public AdminService(IAdminRepository adminRepository)
    {
        _adminRepository = adminRepository;
    }

}
