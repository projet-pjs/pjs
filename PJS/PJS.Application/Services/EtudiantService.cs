﻿using PJS.Application.Services.Interfaces;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Application.Services;

public class EtudiantService : IEtudiantService
{

    private readonly IEtudiantRepository _etudiantRepository;

    public EtudiantService(IEtudiantRepository etudiantRepository)
    {
        _etudiantRepository = etudiantRepository;
    }


    public async Task<List<Etudiant>> GetAll()
    {
        var results = await _etudiantRepository.GetAll();

        return results;
    }


    public async Task<Etudiant?> FindById(int id)
    {
        try
        {
            var results = await _etudiantRepository.FindById(id);

            return results;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }


    public async Task<Etudiant?> EstDansClasseSelectionne(int id, int classeId)
    {
        try
        {
            var result = await _etudiantRepository.EstDansClasseSelectionne(id, classeId);

            return result;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }


    public async Task<List<Etudiant>> FindByClasseId(int classeId)
    {
        try
        {
            var results = await _etudiantRepository.FindByClasseId(classeId);

            return results;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }


    public async Task<Etudiant> Create(Etudiant etudiant)
    {
        try
        {
            var results = await _etudiantRepository.Create(etudiant);

            return results;
        }
        catch (Exception ex)
        {
            Console.WriteLine(ex.Message);
            throw;
        }
    }


    public async Task<bool> Delete(int id)
    {
        var result = await _etudiantRepository.Delete(id);

        return result;
    }
}
