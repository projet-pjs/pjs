﻿using PJS.Application.Services.Interfaces;
using PJS.Data.Repositories.Interfaces;
using PJS.Domain.Models;

namespace PJS.Application.Services;

public class ClasseService : IClasseService
{

    private readonly IClasseRepository _classeRepository;

    public ClasseService(IClasseRepository classeRepository)
    {
        _classeRepository = classeRepository;
    }

    public async Task<List<Classe>> GetAll()
    {
        return await _classeRepository.GetAll();
    }

    public async Task<bool> VerifieClasseEstEncoreOuverte(int classeId)
    {
        return await _classeRepository.VerifieClasseEstEncoreOuverte(classeId);
    }

    public async Task<List<Classe>> FindByEnseignantId(int enseignantId)
    {
        return await _classeRepository.FindByEnseignantId(enseignantId);
    }

    public async Task<Classe?> FindById(int id)
    {
        return await _classeRepository.FindById(id);
    }

    public async Task<Classe?> FindByCodeAcces(string codeAcces)
    {
        return await _classeRepository.FindByCodeAcces(codeAcces);
    }

    public async Task<Classe> Create(Classe classe)
    {
        return await _classeRepository.Create(classe);
    }

    public async Task<bool> Update(Classe classe)
    {
        return await _classeRepository.Update(classe);
    }

    public async Task<bool> Delete(int id)
    {
        return await _classeRepository.Delete(id);
    }

}
