﻿
using PJS.Domain.Models;

namespace PJS.Application.Services.Interfaces;

public interface IClasseService
{
    Task<List<Classe>> GetAll();

    Task<bool> VerifieClasseEstEncoreOuverte(int classeId);

    Task<List<Classe>> FindByEnseignantId(int enseignantId);

    Task<Classe?> FindById(int id);

    Task<Classe?> FindByCodeAcces(string codeAcces);

    Task<Classe> Create(Classe classe);

    Task<bool> Update(Classe classe); 

    Task<bool> Delete(int id);
    
}
