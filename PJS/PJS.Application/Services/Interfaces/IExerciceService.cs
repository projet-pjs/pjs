﻿
using PJS.Domain.Models;

namespace PJS.Application.Services.Interfaces;

public interface IExerciceService
{
    Task<List<Exercice>> FindByClasseId(int classeId);

    Task<List<Exercice>> GetAllExercices();
    
    Task<Exercice> Create(Exercice exercice);
    
}
