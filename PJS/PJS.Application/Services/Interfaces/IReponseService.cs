﻿using PJS.Domain.Models;

namespace PJS.Application.Services.Interfaces;

public interface IReponseService
{

    Task<List<Reponse>> GetReponsesPourExercices(List<int> exerciceIds, int etudiantId);

}
