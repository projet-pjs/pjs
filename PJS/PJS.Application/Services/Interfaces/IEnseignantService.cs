﻿
using PJS.Domain.Models;

namespace PJS.Application.Services.Interfaces;

public interface IEnseignantService
{
    Task<Enseignant?> GetEnseignantParCourrielMdp(string courriel, string motDePasse);
    
    Task<Enseignant> CreerEnseignant(Enseignant enseignant);
}
