﻿using System.ComponentModel.DataAnnotations;

namespace PJS.Application.Dtos.Auth;

#nullable disable
public class ConnexionDto
{
    [EmailAddress]
    public string Courriel { get; set; }

    
    [DataType(DataType.Password)]
    public string MotDePasse { get; set; }
}