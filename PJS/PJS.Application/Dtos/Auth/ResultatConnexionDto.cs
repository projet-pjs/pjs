﻿namespace PJS.Application.Dtos.Auth;

#nullable disable
public class ResultatConnexionDto
{
    public string Token { get; set; }
}