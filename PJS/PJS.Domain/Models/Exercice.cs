﻿
namespace PJS.Domain.Models;

public class Exercice
{
    public int Id { get; set; }

    public string Nom { get; set; } = null!;

    public string? Description { get; set; }

    public int IndexSequence { get; set; }

    public string? ResultatAttenduCompilation { get; set; }

    public string? DemarcheAttendue { get; set; }

    public DateTime? DateEcheance { get; set; }
    
    public int ClasseId { get; set; } 
}
