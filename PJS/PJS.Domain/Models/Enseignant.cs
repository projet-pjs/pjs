﻿
namespace PJS.Domain.Models;

public  class Enseignant
{
    public int Id { get; set; }

    public string Prenom { get; set; } = null!;

    public string Nom { get; set; } = null!;

    public string AdresseCourriel { get; set; } = null!;

    public string MotDePasse { get; set; } = null!;
}
