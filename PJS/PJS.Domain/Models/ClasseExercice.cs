﻿
namespace PJS.Domain.Models;

public class ClasseExercice
{
    public int ClasseId { get; set; }

    public int ExerciceId { get; set; }
}
