﻿
namespace PJS.Domain.Models.AnciensModels;

public class Tache
{
    public int Id { get; set; }

    public string Titre { get; set; } = null!;

    public string Enonce { get; set; } = null!;

    public DateTime Echeance { get; set; }

    public string LangueProgrammation { get; set; } = null!;

    public string NomProfesseur { get; set; } = null!;

    public bool EstTermine { get; set; }

    public int MatiereId { get; set; }

    public Matiere Matiere { get; set; } = null!;

}
