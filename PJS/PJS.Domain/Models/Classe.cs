﻿
namespace PJS.Domain.Models;

public class Classe
{
    public int Id { get; set; }

    public string Nom { get; set; } = null!;

    public string Description { get; set; } = null!;

    public string CodeAcces { get; set; }

    public int EnseignantId { get; set; }
    public bool EstActive { get; set; }
    
}
