﻿
namespace PJS.Domain.Models;

public class Reponse
{
    public int EtudiantId { get; set; }

    public int ExerciceId { get; set; }

    public string CodePJS { get; set; } = null!;

    public string ResultatConsole { get; set; } = null!;

    public bool EstConsulteParEtudiant { get; set; }

    public bool EstConsulteParEnseignant { get; set; }

    public bool EstReussi { get; set; }

    public string? Commentaire { get; set; }
}
