﻿
namespace PJS.Domain.Models;

public class EtudiantClasse
{
    public int ClasseId { get; set; }

    public int EtudiantId { get; set; }
}
