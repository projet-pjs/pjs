﻿
namespace PJS.Domain.Models;

public class Etudiant
{
    public int Id { get; set; }

    public string? Nom { get; set; }
    
    public int ClasseId { get; set; }
}
