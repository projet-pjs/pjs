
// Angular
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from "@angular/common/http";
import { AppRoutingModule } from "./app-routing.module";

// Styles primeng
import { AccordionModule } from 'primeng/accordion';
import { PanelModule } from 'primeng/panel';
import { ButtonModule } from 'primeng/button';
import { RadioButtonModule } from "primeng/radiobutton";
import { DialogModule } from 'primeng/dialog';
import { ToastModule } from 'primeng/toast';
import { MessageService } from 'primeng/api';
import { ConfirmationService } from 'primeng/api';
import { TableModule } from 'primeng/table';
import { PaginatorModule } from 'primeng/paginator';
import { CarouselModule } from 'primeng/carousel';
import { SidebarModule } from 'primeng/sidebar';
import { OverlayPanelModule } from 'primeng/overlaypanel';
import { ProgressSpinnerModule } from 'primeng/progressspinner';
import { TooltipModule } from 'primeng/tooltip';
import { InputTextModule } from 'primeng/inputtext';
import { CheckboxModule } from 'primeng/checkbox';
import { SliderModule } from 'primeng/slider';
import { SelectButtonModule } from 'primeng/selectbutton';
import { MultiSelectModule } from 'primeng/multiselect';
import { CalendarModule } from 'primeng/calendar';
import { TreeModule } from 'primeng/tree';
import { SplitButtonModule } from 'primeng/splitbutton';
import { TagModule } from 'primeng/tag';
import { TreeTableModule } from 'primeng/treetable';
import { ListboxModule } from 'primeng/listbox';
import { OrderListModule } from 'primeng/orderlist';
import { DragDropModule } from 'primeng/dragdrop';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MessagesModule } from 'primeng/messages';

// CodeMirror
import { CodemirrorModule } from '@ctrl/ngx-codemirror';

// Components
import { AppComponent } from './app.component';
import { FenetreDeCodeComponent } from './components/reutilisables/fenetre-de-code/fenetre-de-code.component';
import { NavbarComponent } from './components/reutilisables/navbar/navbar.component';
import { AccueilComponent } from './components/pages/accueil/accueil.component';
import { PseudoDocComponent } from './components/pages/pseudo-doc/pseudo-doc.component';
import { LicenceComponent } from './components/pages/licence/licence.component';
import { HTTP_INTERCEPTORS } from '@angular/common/http';
import { CustomHttpInterceptor } from '../intercepteur/http.intercepteur';
import {ExerciceComponent} from "./components/reutilisables/exercice/exercice.component";
import { PageGestionClassesComponent } from './components/pages/portail-enseignant/page-gestion-classes/page-gestion-classes.component';
import {NgbModule} from "@ng-bootstrap/ng-bootstrap";
import { ConfirmationModalComponent } from './components/reutilisables/confirmation-modal/confirmation-modal.component';

import { ErreurComponent } from './components/reutilisables/erreur/erreur.component';
import { ConnexionEnseignantComponent } from './components/pages/portail-enseignant/connexion-enseignant/connexion-enseignant.component';
import {JwtModule} from "@auth0/angular-jwt";
import { PageCreationClasseComponent } from "./components/pages/portail-enseignant/page-creation-classe/page-creation-classe.component";
import { PageModificationClasseComponent } from './components/pages/portail-enseignant/page-modification-classe/page-modification-classe.component';
import { PageCreationExerciceComponent } from './components/pages/portail-enseignant/page-creation-exercice/page-creation-exercice.component';
import { ListageExerciceEtudiant } from './components/pages/listage-exercice-etudiant/listage-exercice-etudiant.component';
import { ConnexionCompteEtudiant } from './components/modals/connexion-compte-etudiant/connexion-compte-etudiant.component';
import { ConsultationCommentaire } from './components/modals/consultation-commentaire/consultation-commentaire.component';
import { ClasseFermee } from './components/modals/classe-fermee/classe-fermee.component';
import { PageTacheComponent } from './components/pages/page-tache/page-tache.component';


@NgModule({
  declarations: [
    AppComponent,
    FenetreDeCodeComponent,
    NavbarComponent,
    AccueilComponent,
    PseudoDocComponent,
    LicenceComponent,
    PageTacheComponent,
    ExerciceComponent,
    PageGestionClassesComponent,
    ConfirmationModalComponent,
    ErreurComponent,
    ConnexionEnseignantComponent,
    PageCreationClasseComponent,
    PageModificationClasseComponent,
    PageCreationExerciceComponent,
    ListageExerciceEtudiant,
    ConnexionCompteEtudiant,
    ConsultationCommentaire,
    ClasseFermee
  ],
    imports: [
        BrowserModule,
        FormsModule,
        CodemirrorModule,
        AppRoutingModule,
        HttpClientModule,
        AccordionModule,
        PanelModule,
        ButtonModule,
        RadioButtonModule,
        DialogModule,
        ToastModule,
        TableModule,
        PaginatorModule,
        CarouselModule,
        SidebarModule,
        OverlayPanelModule,
        ProgressSpinnerModule,
        TooltipModule,
        InputTextModule,
        CheckboxModule,
        SliderModule,
        SelectButtonModule,
        MultiSelectModule,
        CalendarModule,
        TreeModule,
        DragDropModule,
        SplitButtonModule,
        TagModule,
        TreeTableModule,
        ListboxModule,
        OrderListModule,
        NgbModule,
        ReactiveFormsModule,
        JwtModule.forRoot({
        config: {
          tokenGetter: () => localStorage.getItem('access_token')
        }
      }),
    ],

  providers: [
    ConfirmationService,
    MessageService,
    {
      provide: HTTP_INTERCEPTORS,
      useClass: CustomHttpInterceptor,
      multi: true
    },
  ],
  bootstrap: [
    AppComponent
  ]
})

export class AppModule {

}
