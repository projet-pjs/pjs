import { Component } from '@angular/core';
import { AuthService } from "../../../services/auth.service";

@Component({
  selector: 'app-navbar',
  templateUrl: './navbar.component.html',
  styleUrls: ['./navbar.component.scss']
})
export class NavbarComponent {

  constructor(private authService: AuthService) { }

  /**
   * Obtiens l'utilisateur.
   * @return L'utilisateur.
   */
  get utilisateur() {
    return this.authService.obtenirEnseignant();
  }

  /**
   * Déconnecte d'utilisateur.
   */
  deconnecter() {
    const utilisateur = this.authService.obtenirEnseignant();
    if (utilisateur) {
      this.authService.deconnexion(utilisateur);
    }
  }

}
