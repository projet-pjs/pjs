import { Component, EventEmitter, Input, Output } from '@angular/core';
import { Router } from '@angular/router';
import { Exercice } from "../../../models/exercice";
import { Reponse } from '../../../models/reponse';
import { ClasseService } from '../../../services/classe.service';
import { filter, tap } from 'rxjs';

@Component({
  selector: 'app-exercice',
  templateUrl: './exercice.component.html',
  styleUrls: ['./exercice.component.scss']
})
export class ExerciceComponent {

  /**
   * L'exercice représenté par la composante
   */
  @Input() exercice!: Exercice;

  /**
   * La réponse de l'utilisateur à l'exercice
   */
  @Input() reponse!: Reponse | null;

  /**
   * Identifiant de la classe actuelle
   */
  @Input() classeId!: number;

  /**
   * Si la classe a été fermée
   */
  @Output() classeEstFermee = new EventEmitter<boolean>();

  /**
   * Si le commentaire du professeur doit être affiché
   */
  public commentaireEstAffiche: boolean;

  /**
   * Tous les mois de l'année
   */
  public mois: string[]


  constructor(private router: Router, private classeService: ClasseService) {
    this.commentaireEstAffiche = false;
    this.mois = [
      'janvier',
      'février',
      'mars',
      'avril',
      'mai',
      'juin',
      'juillet',
      'août',
      'septembre',
      'octobre',
      'novembre',
      'decembre'
    ];
  }


  /**
   * Retourne l'énoncé tronqué s'il est trop long
   *
   * @param enonce Énoncé de l'exercice
   * @returns Énoncé tronqué
   */
  public formatEnonce(enonce: string): string {
    return (enonce.length <= 120) ? enonce : enonce.slice(0, 120) + "...";
  }


  /**
   * Rend la date dans un format => 27 septembre à 0h00
   *
   * @param date Date devant être formatée
   * @returns Date formaté
   */
  public formatDate(date: Date): string {
    if (typeof date === 'string') date = new Date(date);

    return (date.getDate().toString() + ' ' +
      this.mois[date.getMonth() + 1] + ' à ' +
      date.getHours().toString() + 'h' +
      date.getMinutes().toString().padStart(2, '0'));
  }


  /**
   * Ouvre le modal contenant le commentaire de l'enseignant sur la copie corrigée
   */
  public ouvreModalCommentaire(): void {
    this.commentaireEstAffiche = true;
  }


  /**
   * Ferme le modal contenant le commentaire de l'enseignant sur la copie corrigée
   *
   * @param $event Si doit fermer
   */
  public fermeModalCommentaire($event: boolean): void {
    this.commentaireEstAffiche = false;
  }


  /**
   * Consulte l'exercice si la classe est encore ouverte
   */
  public consulteExercice(): void {
    this.classeService.verifieClasseEstEncoreOuverte(this.classeId).pipe(
      tap((estOuverte: boolean) => {
        if (!estOuverte) this.classeEstFermee.emit(estOuverte);
      }),
      filter((estOuverte: boolean) => estOuverte),
      tap(() => this.router.navigateByUrl('/exercice/' + this.exercice.id))
    ).subscribe();
  }

}
