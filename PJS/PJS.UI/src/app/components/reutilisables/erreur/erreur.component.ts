import {Component, Input} from '@angular/core';


@Component({
  selector: 'app-erreur',
  template: `
    <div *ngIf="message" class="alert alert-warning alert-dismissible fade show m-3" role="alert">{{message}}</div>
  `,
  styles: []
})

/**
 * Affiche un message d'erreur
 * 
 * @example
 *  <app-erreur [message]="msgErreur"></app-erreur>
 */
export class ErreurComponent {

  @Input() message!: string | null;

}
