import { Component, ViewChild } from '@angular/core';

@Component({
  selector: 'app-fenetre-de-code',
  templateUrl: './fenetre-de-code.component.html',
  styleUrls: ['./fenetre-de-code.component.scss']
})
export class FenetreDeCodeComponent {

  @ViewChild('codeeditor') private codeEditor: { codeMirror: any; } | undefined;

  @ViewChild('codeviewer') private codeViewer: { codeMirror: any; } | undefined;

  public content: string;

  public viewport: string;

  public recharge: boolean;

  public pjsObject: string;

  constructor() {
    this.content = '';
    this.viewport = '';
    this.recharge = false;
    this.pjsObject = "class Util {\n" +
      "rnd(min, max) {\n" +
      "return (Math.random()*(max-min)) + min;\n" +
      "}\n" +
      "empty(list) {\n" +
      "for (let i = list.length; i > 0; i--) {" +
      "list.pop()" +
      "}\n" +
      "}\n" +
      "}\n" +
      "var Pjs = new Util();\n";
  }

  public assigneRechargement(): void {
    this.recharge = !this.recharge;
  }

  public exec(): void {
    window.console.log = this.customLog;

    if (this.codeEditor != undefined) {
      const editor = this.codeEditor.codeMirror;
      const doc = editor.getDoc();

      let result = doc.getValue();
      let intoStr = false;
      let finalResult = '';

      result = result.normalize("NFD").replace(/[\u0300-\u036f]/g, "");
      result = result.split('(').join('( ');
      result = result.split(')').join(' )');
      result = result.split(`'`).join('"');
      result = result.split(/[\n]/).join('@ ');
      result = result.split(/[\s]/);

      for (let i = 0; i < result.length; i++) {
        let current = result[i];

        if (current.startsWith('"')) {
          intoStr = true;
        }

        if (!intoStr) {
          finalResult += this.convertiMotsIndividuels(current.split('@').join(';'));
        } else {
          finalResult += current.split('@').join(';');
        }

        if (current.endsWith('"') || current.endsWith(`";`)) {
          intoStr = false;
        }

        if (current.endsWith('@')) {
          current = current.split('@').join(';')
          finalResult += '\n';
        } else {
          finalResult += ' ';
        }
      }

      finalResult = this.convertPhrases(finalResult);

      if (this.codeViewer != undefined) {
        let viewer = this.codeViewer.codeMirror;
        let doc = viewer.getDoc();
        doc.setValue(finalResult);
      }

      const elm = <HTMLTextAreaElement>document.getElementById('console');
      if (elm != null && this.recharge) {
        elm.value = '';
      }

      finalResult = this.pjsObject + finalResult;
      eval(finalResult);
    }
  }

  private customLog(msg: string): void {
    const elm = <HTMLTextAreaElement>document.getElementById('console');
    if (elm != null) {
      elm.value = msg + '\n' + elm.value;
    }
  }

  private convertiMotsIndividuels(text: string): string {
    let result = text;

    result = result.split('variable').join('var');
    result = result.split('afficher').join('console.log');
    result = result.split('affiche').join('console.log');

    result = result.split('sinon').join('else');
    result = result.split('donc').join('else');
    result = result.split('alors').join('else');
    result = result.split('si').join('if');
    result = result.split('pour').join('for');

    result = result.split('classe').join('class');
    result = result.split('objet').join('class');
    result = result.split('constructeur').join('constructor');
    result = result.split('nouveau').join('new');
    result = result.split('nouvelle').join('new');
    result = result.split('nouvel').join('new');
    result = result.split('ceci').join('this');
    result = result.split('sois-meme').join('this');
    result = result.split('sois').join('this');
    result = result.split('lui-meme').join('this');
    result = result.split('lui').join('this');

    result = result.split('dans').join('of');
    result = result.split('incrementer').join('+=');
    result = result.split('incremente').join('+=');
    result = result.split('increment').join('+=');
    result = result.split('decrementer').join('+=');
    result = result.split('decremente').join('+=');
    result = result.split('décrément').join('+=');
    result = result.split('modulo').join('%');

    result = result.split('.taille').join('.length');
    result = result.split('.longueur').join('.length');
    result = result.split('.count').join('.length');
    result = result.split('.ajouter').join('.push');
    result = result.split('.ajoute').join('.push');
    result = result.split('.ajout').join('.push');
    result = result.split('.inclus').join('.includes');
    result = result.split('.contient').join('.includes');

    result = result.split('arrondi').join('Math.round');
    result = result.split('plafond').join('Math.ceil');
    result = result.split('plancher').join('Math.floor');
    result = result.split('tronquer').join('Math.trunc');
    result = result.split('tronque').join('Math.trunc');

    result = result.split('laitue').join('let');
    result = result.split('laideron').join('let');
    result = result.split('laiderons').join('let');

    return result;
  }

  private convertPhrases(text: string): string {
    let result = text;

    result = result.split('{;').join('{');
    result = result.split('};').join('}');
    result = result.split('+;').join('+');
    result = result.split(';;').join(';');

    result = result.split('nombre aleatoire entre').join('Pjs.rnd');
    result = result.split('vider').join('Pjs.empty');
    result = result.split('vide').join('Pjs.empty');

    result = result.split('plus grand que').join('>');
    result = result.split('plus petit que').join('<');
    result = result.split('plus grand').join('>');
    result = result.split('plus petit').join('<');

    result = result.split('superieur a').join('>');
    result = result.split('superieure a').join('>');

    result = result.split('plus grand ou egale a').join('>=');
    result = result.split('plus petit ou egale a').join('<=');
    result = result.split('plus grand ou egal a').join('>=');
    result = result.split('plus petit ou egal a').join('<=');
    result = result.split('plus grand ou egal').join('>=');
    result = result.split('plus petit ou egal').join('<=');
    result = result.split('plus grand ou egale').join('>=');
    result = result.split('plus petit ou egale').join('<=');
    result = result.split('est inferieur ou egale a').join('<=');
    result = result.split('est superieur ou egale a').join('>=');

    result = result.split('.retirer le premier').join('.shift');
    result = result.split('.retirer premier').join('.shift');
    result = result.split('.retire le premier').join('.shift');
    result = result.split('.retire premier').join('.shift');
    result = result.split('.retirer le dernier').join('.pop');
    result = result.split('.retirer dernier').join('.pop');
    result = result.split('.retire le dernier').join('.pop');
    result = result.split('.retire dernier').join('.pop');

    result = result.split('.convertir en texte').join('.toString');
    result = result.split('.converti en texte').join('.toString');
    result = result.split('.convertir en text').join('.toString');
    result = result.split('.converti en text').join('.toString');
    result = result.split('.en texte').join('.toString');
    result = result.split('.en text').join('.toString');

    result = result.split('.convertir en majuscule').join('.toUpperCase');
    result = result.split('.converti en majuscule').join('.toUpperCase');
    result = result.split('.convertir en minuscule').join('.toLowerCase');
    result = result.split('.converti en minuscule').join('.toLowerCase');
    result = result.split('.en majuscule').join('.toUpperCase');
    result = result.split('.en minuscule').join('.toLowerCase');

    result = result.split('signe de').join('Math.sign');
    result = result.split('signe').join('Math.sign');
    result = result.split('valeur absolue de').join('Math.abs');
    result = result.split('absolu de').join('Math.abs');
    result = result.split('absolu').join('Math.abs');
    result = result.split('exposant').join('Math.pow');
    result = result.split('puissance').join('Math.pow');
    result = result.split('maximum dans').join('Math.max');
    result = result.split('maximum').join('Math.max');
    result = result.split('minimum dans').join('Math.min');
    result = result.split('minimum').join('Math.min');
    result = result.split('nombre aleatoire').join('Math.random');
    result = result.split('aleatoire').join('Math.random');

    result = result.split('est inferieur a').join('<');
    result = result.split('est superieur a').join('>');
    result = result.split('inferieur a').join('<');
    result = result.split('superieur a').join('>');
    result = result.split('inferieur ou egale a').join('<=');
    result = result.split('superieur ou egale a').join('>=');

    result = result.split('tant que').join('while');

    // Mot individuel pouvant faire parti de regroupement
    result = result.split('tant').join('while');
    result = result.split('.majuscule').join('.toUpperCase');
    result = result.split('.minuscule').join('.toLowerCase');
    result = result.split('majuscule').join('.toUpperCase');
    result = result.split('minuscule').join('.toLowerCase');

    // À faire en dernier
    result = result.split('est egale a').join('==');
    result = result.split('est egal a').join('==');
    result = result.split('egale a').join('==');
    result = result.split('egal a').join('==');
    result = result.split('egale').join('==');
    result = result.split('egal').join('==');
    result = result.split('vaut').join('==');
    result = result.split('est').join('==');

    result = result.split('vrai').join('true');
    result = result.split('faux').join('false');
    result = result.split('oui').join('true');
    result = result.split('non').join('false');

    return result
  }

}
