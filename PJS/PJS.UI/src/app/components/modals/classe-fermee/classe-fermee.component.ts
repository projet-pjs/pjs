import { Component, Input } from "@angular/core";
import { Router } from "@angular/router";

@Component({
  selector: 'app-classe-fermee',
  templateUrl: './classe-fermee.component.html',
  styleUrls: ['./classe-fermee.component.scss']
})

export class ClasseFermee {

  /**
   * Si le modale est visible
   */
  @Input() estVisible!: boolean;

  /**
   * Nom de la classe actuelle
   */
  @Input() nomClasse!: string;


  constructor(private router: Router) {

  }


  /**
   * Quitte la classe actuelle
   */
  public quitterClasse(): void {
    this.router.navigate(['/']);
  }

}
