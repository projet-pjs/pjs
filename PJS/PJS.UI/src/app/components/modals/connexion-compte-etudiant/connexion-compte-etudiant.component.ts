import { Component, EventEmitter, Input, Output } from "@angular/core";
import { EtudiantService } from "../../../services/etudiant.service";
import { Etudiant } from "../../../models/Etudiant";
import { GestionMessageService } from "../../../services/gestion-erreur.service";
import { filter, map, mergeMap, tap } from "rxjs";
import { ClasseService } from "../../../services/classe.service";


@Component({
  selector: 'app-connexion-compte-etudiant',
  templateUrl: './connexion-compte-etudiant.component.html',
  styleUrls: ['./connexion-compte-etudiant.component.scss']
})
export class ConnexionCompteEtudiant {

  /**
   * Si le modal est visible
   */
  @Input() estVisible!: boolean;

  /**
   * Identifiant de la classe auquel l'étudiant doit se connecter
   */
  @Input() classeId!: number;

  /**
   * Si la classe a été fermée
   */
  @Output() classeEstFermee = new EventEmitter<boolean>();

  /**
   * Compte de l'utilisateur ayant été créé
   */
  @Output() etudiant = new EventEmitter<Etudiant | null>();

  /**
   * Nom d'utilisateur saisie
   */
  public nomUtilisateur: string;

  /**
   * Bloque le bouton si la connexion est en cours
   */
  public bloqueBouton: boolean;

  constructor(private etudiantService: EtudiantService, private classeService: ClasseService, private gestionMessageService: GestionMessageService) {
    this.nomUtilisateur = '';
    this.bloqueBouton = false;
  }


  /**
   * Retourne si le nom saisie par l'étudiant n'est pas adéquat
   * 
   * @returns Si invalide
   */
  public estInvalide(): boolean {
    return (this.nomUtilisateur.trim() === '') || (this.nomUtilisateur == null);
  }


  /**
   * Retourne à l'écran d'accueil
   */
  public quitterClasse(): void {
    this.etudiant.next(null);
  }


  /**
   * Inscrit l'étudiant au cours
   */
  public inscritAuCours(): void {
    this.bloqueBouton = true;
    this.classeService.verifieClasseEstEncoreOuverte(this.classeId).pipe(
      tap((estOuverte: boolean) => {
        if (!estOuverte) {
          this.classeEstFermee.emit(estOuverte);
          setTimeout(() => this.etudiant.next(null), 700);
        }
      }),
      filter((estOuverte: boolean) => estOuverte),
      mergeMap(() => {
        return this.etudiantService.create(this.classeId, this.nomUtilisateur).pipe(
          map((etudiant: Etudiant) => this.etudiant.next(etudiant))
        )
      })
    ).subscribe({
      next: () => this.bloqueBouton = false,
      error: () => {
        this.bloqueBouton = false;
        this.gestionMessageService.lanceErreur('Impossible de créer un compte');
      }
    });
  }

}
