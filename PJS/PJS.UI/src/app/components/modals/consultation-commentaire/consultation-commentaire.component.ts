import { Component, EventEmitter, Input, Output } from "@angular/core";

@Component({
  selector: 'app-consultation-commentaire',
  templateUrl: './consultation-commentaire.component.html',
  styleUrls: ['./consultation-commentaire.component.scss']
})

export class ConsultationCommentaire {

  /**
   * Si le modal est visible
   */
  @Input() estVisible!: boolean;

  /**
   * Nom de la classe actuelle
   */
  @Input() nomClasse!: string;

  /**
   * Contenu du commentaire inscrit par l'enseignant
   */
  @Input() contenuCommentaire!: string | null;

  /**
   * Si le modal doit fermer
   */
  @Output() doitFermer = new EventEmitter<boolean>();


  constructor() {

  }


  /**
   * Ferme le modale
   */
  public quitterModal(): void {
    this.doitFermer.next(true);
  }

}
