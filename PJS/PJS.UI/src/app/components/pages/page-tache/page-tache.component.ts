import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { AncienService } from '../../../services/ancien/ancien.service';

@Component({
  selector: 'app-page-tache',
  templateUrl: './page-tache.component.html',
  styleUrls: ['./page-tache.component.scss']
})
export class PageTacheComponent implements OnInit {

  private id: string;

  public tache!: any;

  constructor(public ancienService: AncienService, private route: ActivatedRoute) {
    this.id = '';
  }

  ngOnInit() {
    this.route.params.subscribe(params => {
      this.id = params['id'];
      this.ancienService.getTacheById(Number(this.id)).subscribe((tache: any) => {
        this.tache = tache;
      })
    })
  }

}
