import { Component, OnInit } from '@angular/core';
import { ClasseService } from '../../../services/classe.service';
import { ActivatedRoute, Router } from '@angular/router';
import { Classe } from '../../../models/Classe';
import { Observable, catchError, concatMap, filter, forkJoin, map, mergeMap, of, tap } from 'rxjs';
import { ReponseService } from '../../../services/reponse.service';
import { ExerciceService } from '../../../services/exercice.service';
import { Etudiant } from '../../../models/Etudiant';
import { EtudiantService } from '../../../services/etudiant.service';
import { Reponse } from '../../../models/reponse';
import { GestionMessageService } from '../../../services/gestion-erreur.service';
import { Exercice } from '../../../models/exercice';

@Component({
  selector: 'app-root',
  templateUrl: './listage-exercice-etudiant.component.html',
  styleUrls: ['./listage-exercice-etudiant.component.scss']
})

export class ListageExerciceEtudiant implements OnInit {

  private codeCours: string;

  private localStorageKey: string;

  public classe: Classe | null;

  public etudiant: Etudiant | null;

  public exercices: Exercice[];

  public reponses: Reponse[];

  public doitAfficheModalConnexion: boolean;

  public doitAfficheModalClasseFermee: boolean;

  public tourneBouton: boolean;

  constructor(private route: ActivatedRoute, private router: Router, private classeService: ClasseService, private exerciceService: ExerciceService, private reponseService: ReponseService, private etudiantService: EtudiantService, private gestionMessageService: GestionMessageService) {
    this.localStorageKey = 'compteUtilisateurPJS';
    this.etudiant = null;
    this.codeCours = '';
    this.classe = null;
    this.exercices = [];
    this.reponses = [];
    this.doitAfficheModalConnexion = false;
    this.doitAfficheModalClasseFermee = false;
    this.tourneBouton = false;
  }


  ngOnInit(): void {
    this.route.params.pipe(
      tap((params) => this.codeCours = params['code']),
      concatMap(() => this.recupereClasse()),
      filter(() => !this.doitAfficheModalClasseFermee),
      concatMap(() => forkJoin([
        this.recupereExercices(),
        this.recupereEtudiant()
      ])),
      tap(() => this.doitAfficheModalConnexion = (this.etudiant === null)),
      filter(() => !this.doitAfficheModalConnexion),
      concatMap(() => this.recupereReponses())
    ).subscribe();
  }


  /**
   * Récupère la classe correspondant au code reçu
   *
   * @returns Observable
   */
  private recupereClasse(): Observable<any> {
    return this.classeService.getClasseByCodeAcces(this.codeCours).pipe(
      catchError((erreur) => {
        this.gestionMessageService.lanceErreur('Impossible de récupérer la liste d\'exercice');
        throw erreur;
      }),
      map((classe: Classe) => {
        this.classe = classe;
        this.doitAfficheModalClasseFermee = (this.classe == null) || !(this.classe.estActive);
        return classe;
      })
    );
  }


  /**
   * Récupère les exercices de la classe
   *
   * @returns Observable
   */
  private recupereExercices(): Observable<any> {
    if (!this.classe) return of();

    return this.exerciceService.findByClasseId(this.classe.id).pipe(
      catchError((erreur) => {
        this.gestionMessageService.lanceErreur('Impossible de récupérer la liste d\'exercice');
        throw erreur;
      }),
      tap((exercices: Exercice[]) => this.exercices = exercices)
    );
  }


  /**
   * Récupère le dernier étudiant ayant été stocké dans le local storage
   *
   * Assigne ce compte seulement s'il est encore présent dans la base de donnée
   * (car les comptes ne sont plus stockés lors d'une fermeture de classe)
   *
   * @returns Observable
   */
  private recupereEtudiant(): Observable<any> {
    const json = localStorage.getItem(this.localStorageKey);
    if (!json || !this.classe) return of(null);

    const dernierEtudiant = JSON.parse(json) as Etudiant;
    return this.etudiantService.estDansClasseSelectionne(dernierEtudiant.id, this.classe.id).pipe(
      catchError((erreur) => {
        this.gestionMessageService.lanceErreur('Impossible de charger le dernier compte utilisateur ayant été utilisé');
        throw erreur;
      }),
      tap((etudiant) => {
        this.etudiant = etudiant;
        if (!this.etudiant) localStorage.removeItem(this.localStorageKey);
      })
    );
  }


  /**
   * Récupère les réponses de l'utilisateur
   *
   * @returns Observable
   */
  private recupereReponses(): Observable<any> {
    if (!this.etudiant) return of();

    return of(this.exercices).pipe(
      map((exercices: Exercice[]) => exercices.map(exercice => exercice.id)),
      mergeMap((exerciceIds: number[]) => this.reponseService.getReponsesPourExercices(exerciceIds, this.etudiant!.id)),
      catchError(() => {
        this.gestionMessageService.lanceErreur('Impossible de récupérer les réponses');
        return of();
      }),
      tap((reponses: Reponse[]) => this.reponses = reponses)
    );
  }


  /**
   * Reponse lorsque le modale de connexion est fermé
   *
   * @param etudiant L'étudiant ayant été créé
   */
  public onUtilisateurConnecte(etudiant: Etudiant | null): void {
    this.etudiant = etudiant;
    this.doitAfficheModalConnexion = false;
    if (etudiant) {
      localStorage.setItem(this.localStorageKey, JSON.stringify(etudiant));
    } else {
      localStorage.removeItem(this.localStorageKey);
      if (!this.doitAfficheModalClasseFermee) this.router.navigate(['/']);
    }
  }


  /**
   * Reponse lors d'une connexion d'étudiant à savoir si la classe est bien encore ouverte
   *
   * @param estOuverte Si la classe est ouverte
   */
  public onConnexionEtatClasse(estOuverte: boolean): void {
    this.doitAfficheModalClasseFermee = !estOuverte;
  }


  /**
   * Rafraichi les données de la page (effectué manuellement par l'utilisateur)
   *
   * IMPORTANT:
   *    1. S'il a pu rafraichir manuellement la page, c'est qu'il possédait un compte
   *       Si son compte n'existe plus, c'est que la classe a été fermée entre-temps.
   *
   *    2. Cette validation est utilisé plutôt que "classeService.verifieClasseEstEncoreOuverte()"
   *       pour rafraichir par le fait même le compte de l'étudiant au cas ou il serait mal chargé.
   *
   *    3. Les étapes ne sont pas dans le même ordre de priorité que dand "ngOnInit()".
   */
  public rafraichiPage(): void {
    this.bloqueEtTourneBouton();
    this.recupereEtudiant().pipe(
      tap(() => this.doitAfficheModalClasseFermee = (this.etudiant === null)),
      filter(() => !this.doitAfficheModalClasseFermee),
      concatMap(() => this.recupereExercices()),
      concatMap(() => this.recupereReponses())
    ).subscribe({
      next: () => {
        this.gestionMessageService.lanceSucces('Rafraichissement effectuée avec succès');
      },
      error: () => {
        this.gestionMessageService.lanceErreur('Impossible de rafraichir les données');
      }
    })
  }


  /**
   * Tourne le bouton de rafraichissement et le rend désactivé pour un instant
   */
  private bloqueEtTourneBouton(): void {
    this.tourneBouton = true;
    setTimeout(() => {
      this.tourneBouton = false;
    }, 3000)
  }


  /**
   * Retourne la réponse de l'utilisateur
   *
   * @param exerciceId Identifiant de l'exercice
   * @returns Si contient une réponse
   */
  public prendReponseExercice(exerciceId: number): Reponse | null {
    return this.reponses.filter(x => x.exerciceId == exerciceId)[0] ?? null;
  }

}
