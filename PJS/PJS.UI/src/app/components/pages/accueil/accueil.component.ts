import { Component } from '@angular/core';
import { Classe } from '../../../models/Classe';
import { Router } from '@angular/router';
import { ClasseService } from '../../../services/classe.service';
import { AuthService } from "../../../services/auth.service";

@Component({
  selector: 'app-accueil',
  templateUrl: './accueil.component.html',
  styleUrls: ['./accueil.component.scss']
})
export class AccueilComponent {

  /**
   * Message d'erreur
   */
  public msgErreur: string | null;

  /**
   * Champ texte
   */
  public code: string;

  /**
   * Constructeur
   *
   * @param classService
   * @param authService
   * @param router
   */
  constructor(private classService: ClasseService, private authService: AuthService, private router: Router) {
    this.msgErreur = null;
    this.code = '';
  }
  
  /**
   * Rejoint une classe
   */
  public rejoindreClasse(): void {
    if (this.code.length > 0) {
      this.classService.getClasseByCodeAcces(this.code).subscribe({
        next: (classe: Classe) => {
          if (classe) {
            this.router.navigate(['exercices', this.code]);
          } else {
            this.msgErreur = 'Impossible de trouver la classe';
          }
        },
        error: () => {
          this.msgErreur = 'Impossible de trouver la classe';
        }
      });
    }
  }

  /**
   * Obtiens l'utilisateur
   * 
   * @return L'utilisateur
   */
  get utilisateur() {
    return this.authService.obtenirEnseignant();
  }

}
