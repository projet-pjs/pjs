import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from "@angular/forms";
import { Router } from "@angular/router";
import {ConnexionDto} from "../../../../models/Dtos/Connexion.dto";
import {AuthService} from "../../../../services/auth.service";
import {ResultatConnexionDto} from "../../../../models/Dtos/resultatConnexion.dto";

@Component({
  selector: 'app-connexion-enseignant',
  templateUrl: './connexion-enseignant.component.html',
  styleUrl: './connexion-enseignant.component.css'
})
export class ConnexionEnseignantComponent {


  /**
   * Message d'erreur
   */
  public msgErreur: string | null;

  /**
   * Formulaire d'entrée pour la connexion d'un enseignant
   */
  public connexionEnseignantForm: FormGroup;

  /**
   * Obtiens le courriel
   *
   * @return Courriel
   */
  get courriel() {
    return this.connexionEnseignantForm.get('courriel');
  }

  /**
   * Obtiens le mot de passe
   * @return Mot de passe
   */
  get motDePasse() {
    return this.connexionEnseignantForm.get('motDePasse');
  }

  constructor(private authService: AuthService, private fb: FormBuilder, private router: Router) {
    this.msgErreur = null;

    this.connexionEnseignantForm = this.fb.group({
      courriel: ['', [Validators.required, Validators.email]],
      motDePasse: ['', [Validators.required]]
    }, {
      updateOn: 'submit'
    });
  }



  public connecterEnseignant(): void {
    if(this.connexionEnseignantForm.valid) {
      const enseignant = this.connexionEnseignantForm.value as ConnexionDto;
      this.authService.connexion(enseignant).subscribe({
        next: (reponse: ResultatConnexionDto) => {
          if (reponse) {
            localStorage.setItem("access_token", reponse.token);
            this.router.navigate(['portail-enseignant/gestion']);
          } else {
            this.msgErreur = "La combinaison de l'adresse courriel et du mot de passe est incorrect.";
          }
        },
        error: () => {
          this.msgErreur = "La combinaison de l'adresse courriel et du mot de passe est incorrect.";
        }
      });
    }
  }

}
