import { Component } from '@angular/core';
import {ActivatedRoute} from "@angular/router";

@Component({
  selector: 'app-page-modification-classe',
  templateUrl: './page-modification-classe.component.html',
  styleUrl: './page-modification-classe.component.css'
})
export class PageModificationClasseComponent {

  constructor(private route: ActivatedRoute) {
  }
}
