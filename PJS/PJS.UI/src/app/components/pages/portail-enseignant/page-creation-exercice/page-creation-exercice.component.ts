import {Component, ViewChild} from '@angular/core';
import {Exercice} from "../../../../models/exercice";
import {FormBuilder, Validators} from "@angular/forms";
import {Router} from "@angular/router";
import {ClasseExercicesStoreService} from "../../../../services/classe-exercices-store.service";

@Component({
  selector: 'app-page-creation-exercice',
  templateUrl: './page-creation-exercice.component.html',
  styleUrl: './page-creation-exercice.component.css'
})
export class PageCreationExerciceComponent {

  /**
   * Message d'erreur
   */
  public msgErreur: string | null;

  public content: string;

  @ViewChild('codeeditor') private codeEditor: { codeMirror: any; } | undefined;

  constructor(private fb: FormBuilder,
              private classeExercicesStore: ClasseExercicesStoreService,
              private router: Router) {
    this.msgErreur = null;
    this.content = '';
  }

  /**
   * Obtiens le nom de l'exercice.
   * @return le nom de l'exercice.
   */
  get nom() {
    return this.exerciceForm.get('nom');
  }

  /**
   * Obtiens la l'énoncé de l'exercice.
   * @return la l'énoncé de l'exercice.
   */
  get enonce() {
    return this.exerciceForm.get('enonce');
  }

  /**
   * Obtiens le résultat attendu de l'exercice.
   * @return le résultat attendu de l'exercice.
   */
  get resultat() {
    return this.exerciceForm.get('resultat');
  }

  /**
   * Formulaire de création d'un exercice
   */
  exerciceForm = this.fb.group({
    nom: ['', [Validators.required]],
    enonce: ['', [Validators.required]],
    resultat: [],
  }, {
    updateOn: 'blur'
  });

  public creerExercice(): void {
    if(this.exerciceForm.valid) {
      const exercice = new Exercice();
      exercice.nom = this.exerciceForm.value.nom as string;
      exercice.description = this.exerciceForm.value.enonce as string;
      const resultat = this.exerciceForm.value.resultat;
      if(resultat != null || resultat != undefined) {
        exercice.resultatAttenduCompilation = resultat;
      } else {
        exercice.resultatAttenduCompilation = '';
      }
      if (this.codeEditor != undefined) {
        const demarche = this.codeEditor.codeMirror.getDoc().getValue();
        exercice.demarcheAttendu = demarche;
      }
      exercice.indexSequence = this.classeExercicesStore.exercices.length + 1;
      this.classeExercicesStore.ajouterExercice(exercice);
      this.router.navigate(["portail-enseignant/nouvelle-classe"])
    } else {
      this.msgErreur = "L'exercice doit minimalement contenir un titre et un énoncé.";
    }

  }

}
