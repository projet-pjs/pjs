import { Component } from '@angular/core';
import {FormBuilder, Validators} from "@angular/forms";
import {Exercice} from '../../../../models/exercice';
import {Classe} from "../../../../models/Classe";
import {ClasseService} from "../../../../services/classe.service";
import {Router} from "@angular/router";
import {ClasseExercicesStoreService} from "../../../../services/classe-exercices-store.service";
import {ExerciceService} from "../../../../services/exercice.service";
import {AuthService} from "../../../../services/auth.service";

@Component({
  selector: 'app-page-creation-classe',
  templateUrl: './page-creation-classe.component.html',
  styleUrl: './page-creation-classe.component.css'
})
export class PageCreationClasseComponent {


  /**
   * Message d'erreur
   */
  public msgErreur: string | null;

  /**
   * Liste d'exercices de la classe en mémoire
   */
  exercices: Exercice[];

  async ngOnInit() {
    if(this.classeExercicesStore.classe != undefined) {
      this.exercices = this.classeExercicesStore.exercices;
      this.classeForm.patchValue(this.classeExercicesStore.classe);
    }
  }

  constructor(private fb: FormBuilder,
              private classeService: ClasseService,
              private exerciceService: ExerciceService,
              private authService: AuthService,
              private classeExercicesStore: ClasseExercicesStoreService,
              private router: Router) {
    this.exercices = this.classeExercicesStore.exercices;
    this.msgErreur = null;
  }


  /**
   * Obtiens le nom de la classe
   * @return le nom de la classe.
   */
  get nom() {
    return this.classeForm.get('nom');
  }

  /**
   * Obtiens la description de la classe.
   * @return la description de la classe.
   */
  get description() {
    return this.classeForm.get('description');
  }

  /**
   * Formulaire pour la création d'une classe.
   */
  classeForm = this.fb.group({
    nom: ['', [Validators.required]],
    description: ['']
  }, {
    updateOn: 'blur'
  });

  /**
   * Permets de créer une classe
   */
  public creerClasse(): void {
    if(this.classeForm.valid) {
      const classe = new Classe();
      classe.nom = this.classeForm.value.nom as string;
      const description = this.classeForm.value.description;
      if(description != null || description != undefined) {
        classe.description = description;
      } else {
        classe.description = '';
      }
      classe.codeAcces = this.creerCodeAcces();
      const enseignant = this.authService.obtenirEnseignant();
      if(enseignant != null) {
        classe.enseignantId = enseignant.id;
      }
      classe.estActive = false;
      this.classeService.createClasse(classe).subscribe( {
        next: (response: Classe) => {
          for (const exercice of this.exercices) {
            exercice.classeId = response.id;
            this.exerciceService.createExercice(exercice).subscribe();
          }
          this.classeExercicesStore.viderMemoire();
          this.router.navigate(["portail-enseignant/gestion"]);
        },
        error: () => {
          this.msgErreur = "Une erreur est survenue lors de la création de la classe.";
        }
      });
    } else {
      this.msgErreur = "La classe doit avoir un nom et contenir un exercice au minimum.";
    }
  }

  /**
   * Permets d'ajouter un exercice dans la liste temporaire
   */
  public ajouterExercice() {
    this.stockerClasse();
    this.router.navigate(["portail-enseignant/nouvel-exercice"]);
  }

  /**
   * Permets de stocker une classe dans le magasin
   * @private
   */
  private stockerClasse() {
    const classe = new Classe();
    classe.nom = this.classeForm.value.nom as string;
    const description = this.classeForm.value.description;
    if(description != null || description != undefined) {
      classe.description = description;
    } else {
      classe.description = '';
    }
    this.classeExercicesStore.classe = classe;
  }

  /**
   * Permets de créer un code d'accès pour une classe (Possiblement un risque de collision)
   */
  creerCodeAcces() {
    let code = '';
    const caracteres = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';
    const longueurCaracteres = caracteres.length;
    let nombreTours = 0;
    while (nombreTours < 10) {
      code += caracteres.charAt(Math.floor(Math.random() * longueurCaracteres));
      nombreTours += 1;
    }
    return code;
  }

  /**
   * Permets d'annuler la création d'une classe et vider les données en mémoire
   */
  annulerCreation() {
    this.classeExercicesStore.viderMemoire();
    this.router.navigate(["portail-enseignant/gestion"]);
  }
}
