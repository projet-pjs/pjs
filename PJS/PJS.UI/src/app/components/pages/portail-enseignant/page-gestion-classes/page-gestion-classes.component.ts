import { Component } from '@angular/core';
import {Classe} from "../../../../models/Classe";
import {ActivatedRoute, Router} from "@angular/router";
import {NgbModal} from "@ng-bootstrap/ng-bootstrap";
import {ConfirmationModalComponent} from "../../../reutilisables/confirmation-modal/confirmation-modal.component";
import {ClasseService} from "../../../../services/classe.service";
import {AuthService} from "../../../../services/auth.service";
import {Enseignant} from "../../../../models/enseignant";

@Component({
  selector: 'app-page-gestion-classes',
  templateUrl: './page-gestion-classes.component.html',
  styleUrl: './page-gestion-classes.component.css'
})
export class PageGestionClassesComponent {

  /**
   * Message d'erreur. (peut être null)
   * @type {string}
   */
  msgErreur?: string;

  enseignant?: Enseignant | null;

  classes: Classe[];

  constructor(private classeService: ClasseService,
              private authService: AuthService,
              private modalService: NgbModal,
              private router: Router) {
    this.classes = [];
  }

  ngOnInit(): void {
    this.enseignant = this.authService.obtenirEnseignant();
    if(this.enseignant != null) {
      this.classeService.getClassesByEnseignantId(this.enseignant.id).subscribe({
        next:(classes: Classe[]) => {
          this.classes = classes;
         },
        error: () => {
          this.msgErreur = "Impossible d'obtenir les classes de l'enseignant.";
        }
        });
    } else {
      this.msgErreur = "Aucun enseignant n'est connecté.";
    }
  }
  async supprimerClasse(id: number) {

      try {
        const modal = this.modalService.open(ConfirmationModalComponent);
        modal.componentInstance.messageConfirmation = 'Êtes-vous certain de vouloir supprimer cette classe?';
        const result = await modal.result;
        if (result) {
          this.classeService.deleteClasse(id);
          // (valider le retour et si true, fermer la modale)
        }
      } catch (error) {
        this.msgErreur = "Impossible de supprimer la classe. Une erreur est survenue.";
      }
  }
  async modifierClasse(id: number) {
    this.router.navigate(['portail-enseignant/modification-classe', id])
  }

  async arreterClasse(id: number) {

  }

}
