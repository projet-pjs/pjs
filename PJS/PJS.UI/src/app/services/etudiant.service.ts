import { Injectable } from "@angular/core";
import { HttpRequestService } from "./http-request-service/http-request.service";
import { Observable } from "rxjs";
import { Etudiant } from "../models/Etudiant";

@Injectable({
  providedIn: 'root'
})


export class EtudiantService {

  private nomControlleur: string;

  constructor(private httpRequestService: HttpRequestService) {
    this.nomControlleur = 'Etudiant';
  }


  public findById(id: number): Observable<Etudiant> {
    return this.httpRequestService.httpGet(this.nomControlleur, 'findById', { id: id });
  }

  public estDansClasseSelectionne(id: number, classeId: number): Observable<Etudiant> {
    return this.httpRequestService.httpGet(this.nomControlleur, 'estDansClasseSelectionne', { id: id, classeId: classeId });
  }
  
  public create(classeId: number, nom: string): Observable<Etudiant> {
    return this.httpRequestService.httpPost(this.nomControlleur, 'create', { nom: nom, classeId: classeId });
  }

}
