import { Injectable } from "@angular/core";
import { HttpRequestService } from "./http-request-service/http-request.service";
import { Classe } from "../models/Classe";
import { Observable } from "rxjs";
import {ConnexionDto} from "../models/Dtos/Connexion.dto";
import {ResultatConnexionDto} from "../models/Dtos/resultatConnexion.dto";
import {HttpClient} from "@angular/common/http";

@Injectable({
  providedIn: 'root'
})

export class ClasseService {

  private defaultControllerName: string;

  constructor(private httpRequestService: HttpRequestService) {
    this.defaultControllerName = 'Classe';
  }

  /**
   * Obtiens une classe selon son identifiant
   *
   * @param id Identifiant d'une classe
   * @return Classe
   */
  public getClasseParId(id: number): Observable<Classe> {
    return this.httpRequestService.httpGet(this.defaultControllerName, 'FindById', { id });
  }

  /**
   * Obtiens une classe selon son code d'accès
   *
   * @param codeAcces Code d'accès de la classe
   * @returns Classe
   */
  public getClasseByCodeAcces(codeAcces: string): Observable<Classe> {
    return this.httpRequestService.httpGet(this.defaultControllerName, 'FindByCodeAcces', { codeAcces });
  }

  /**
   * Vérifie si la classe est encore ouverte
   * 
   * @param classeId Identifiant d'une classe
   * @returs Si ouverte
   */
  public verifieClasseEstEncoreOuverte(classeId: number): Observable<boolean> {
    return this.httpRequestService.httpGet(this.defaultControllerName, 'VerifieClasseEstEncoreOuverte', { classeId: classeId });
  }

  /**
   * Obtiens une classe selon son code d'accès
   *
   * @param enseignantId Identifiant d'un enseignant
   * @returns Classe
   */
  public getClassesByEnseignantId(enseignantId: number): Observable<Classe[]> {
    return this.httpRequestService.httpGet('Classe', 'FindByEnseignantId', { enseignantId });
  }


  /**
   * Retourne la classe créée
   *
   * @param classe
   * @returns Classe
   */
  public createClasse(classe : Classe): Observable<Classe> {
    return this.httpRequestService.httpPost('Classe', 'Create', classe);
  }

  /**
   * Retourne vrai si la classe est à jour
   *
   * @param classe
   * @returns Classe
   */
  public updateClasse(classe : Classe): Observable<boolean> {
    return this.httpRequestService.httpPatch('Classe', 'Update', classe);
  }

  /**
   * Retourne vrai si la classe est supprimé
   *
   * @param id
   * @returns Classe
   */
  public deleteClasse(id : number): Observable<boolean> {
    return this.httpRequestService.httpDelete('Classe', 'Delete', id);
  }


}
