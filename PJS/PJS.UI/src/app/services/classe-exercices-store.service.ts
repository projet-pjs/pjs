import { Injectable } from '@angular/core';
import {Exercice} from "../models/exercice";
import {Classe} from "../models/Classe";

/**
 * Service servant à la conservation des valeurs lors de la création d'une nouvelle classe.
 * Garde en mémoire la classe et les exercices durant les changements de pages.
 */
@Injectable({
  providedIn: 'root'
})
export class ClasseExercicesStoreService {

  exercices: Exercice[];

  classe?: Classe;
  constructor() {
    this.exercices = [];
    this.classe = undefined;
  }

  public ajouterExercice(exercice: Exercice) {
    this.exercices.push(exercice);
  }

  public retirerExercice(exercice: Exercice) {
    let index = this.exercices.indexOf(exercice);
    delete this.exercices[index];
  }

  public viderMemoire() {
    this.exercices = [];
    this.classe = undefined;
  }
}
