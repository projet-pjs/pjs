import { Injectable } from "@angular/core";
import { HttpRequestService } from "./http-request-service/http-request.service";
import { MessageService } from "primeng/api";

@Injectable({
  providedIn: 'root'
})

export class GestionMessageService {


  constructor(private httpRequestService: HttpRequestService, private messageService: MessageService) {

  }


  /**
   * Lance un message d'erreur (PrimeNg)
   * 
   * @param message Message à afficher
   */
  public lanceErreur(message: string) {
    this.messageService.add({ severity: 'error', summary: 'Warning', detail: message });
    console.error(message);
  }

  /**
   * Lance un message de succes (PrimeNg)
   * 
   * @param message Message à afficher
   */
  public lanceSucces(message: string) {
    this.messageService.add({ severity: 'success', summary: 'Succès', detail: message });
  }
}
