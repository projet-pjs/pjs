import { Injectable } from "@angular/core";
import { HttpRequestService } from "./http-request-service/http-request.service";
import { Reponse } from "../models/reponse";
import { Observable } from "rxjs";

@Injectable({
  providedIn: 'root'
})


export class ReponseService {

  private nomControlleur: string;

  constructor(private httpRequestService: HttpRequestService) {
    this.nomControlleur = 'Reponse';
  }

  public getReponsesPourExercices(exerciceIds: number[], etudiantId: number): Observable<Reponse[]> {
    return this.httpRequestService.httpGet(this.nomControlleur, 'getReponsesPourExercices', {
      exerciceIds: exerciceIds,
      etudiantId: etudiantId
    });
  }
}
