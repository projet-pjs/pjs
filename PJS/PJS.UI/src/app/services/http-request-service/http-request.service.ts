import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Observable } from 'rxjs';
import { environment } from '../../../environments/environment';

@Injectable({
  providedIn: 'root'
})

export class HttpRequestService {

  private baseUrl: string;

  constructor(private http: HttpClient) {
    this.baseUrl = `${environment.apiUrl}/api`;
  }


  public httpGet(controller: string, method: string, params: any = null): Observable<any> {
    const url = `${this.baseUrl}/${controller}/${method}`;

    const response = this.http.get(url, {
      params: params,
      responseType: 'json',
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });

    return response;
  }


  public httpPost(controller: string, method: string, body: any = null): Observable<any> {
    const url = `${this.baseUrl}/${controller}/${method}`;

    const requestBody = (body !== null) ? JSON.stringify(body) : null;

    const result = this.http.post(url, requestBody, {
      responseType: 'json',
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });

    return result;
  }

  public httpPatch(controller: string, method: string, body: any = null): Observable<any> {
    const url = `${this.baseUrl}/${controller}/${method}`;

    const requestBody = (body !== null) ? JSON.stringify(body) : null;

    const result = this.http.patch(url, requestBody, {
      responseType: 'json',
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });

    return result;
  }

  public httpDelete(controller: string, method: string, params: any = null): Observable<any> {
    const url = `${this.baseUrl}/${controller}/${method}`;

    const response = this.http.delete(url, {
      params: params,
      responseType: 'json',
      headers: new HttpHeaders({ 'Content-Type': 'application/json' })
    });

    return response;
  }

}
