import { Observable } from "rxjs";
import { Injectable } from "@angular/core";
import { HttpRequestService } from "../http-request-service/http-request.service";

@Injectable({
  providedIn: 'root'
})

/**
 *
 * ANCIEN SERVICE DE TOUT
 *
 * MIS À JOUR À TITRE D'EXEMPLE
 * SERT SIMPLEMENT À MONTRER COMMENT APPELER L'API AVEC DES APPELS
 *
 */
export class AncienService {

  constructor(private httpService: HttpRequestService) {
  }


  public getMatieres(): Observable<any[]> {
    return this.httpService.httpGet('Application', 'GetAllMatieres', null);
  }


  public getTaches(): Observable<any[]> {
    return this.httpService.httpGet('Application', 'GetAllTaches', null);
  }


  public getTachesByMatiereId(id: number): Observable<any[]> {
    return this.httpService.httpGet('Application', 'GetTachesByMatiereId', id);
  }


  public getMatiereById(id: number): Observable<any> {
    return this.httpService.httpGet('Application', 'GetMatiereById', { id: id });
  }


  public getTacheById(id: number): Observable<any> {
    return this.httpService.httpGet('Application', 'GetTacheById', { id });
  }



  public getClasseParId(id: number): Observable<any> {
    return this.httpService.httpPost('Application', 'GetClasseParId', { id: id });
  }


}
