import { Injectable } from "@angular/core";
import { HttpRequestService } from "./http-request-service/http-request.service";
import {Observable} from "rxjs";
import {Exercice} from "../models/exercice";



@Injectable({
  providedIn: 'root'
})


export class ExerciceService {

  constructor(private httpRequestService: HttpRequestService) {

  }

  /**
   * Retourne une exercice
   *
   * @param exercice
   * @returns Exercice
   */
  public createExercice(exercice : Exercice): Observable<Exercice> {
    return this.httpRequestService.httpPost('Exercice', 'Create', exercice);
  }

  /**
   * Obtiens les exercices d'une classe
   *
   * @param classeId Identifiant d'une classe
   * @return Liste d'exercices correspondant à une classe
   */
  public findByClasseId(classeId: number): Observable<Exercice[]> {
    return this.httpRequestService.httpGet('Exercice', 'findByClasseId', { classeId: classeId });
  }


}
