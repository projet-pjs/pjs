import { Injectable } from '@angular/core';
import {Enseignant} from "../models/enseignant";
import {Router} from "@angular/router";
import {JwtHelperService} from "@auth0/angular-jwt";
import {JwtPayloadDto} from "../models/Dtos/jwt-payload.dto";
import {ConnexionDto} from "../models/Dtos/Connexion.dto";
import {Observable} from "rxjs";
import {HttpRequestService} from "./http-request-service/http-request.service";
import {ResultatConnexionDto} from "../models/Dtos/resultatConnexion.dto";

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  constructor(private router: Router,
              private jwtHelper: JwtHelperService,
              private httpRequestService: HttpRequestService) { }

  /**
   * Retourne un enseignant
   *
   * @param connexionDto
   * @returns Enseignant
   */
  public connexion(connexionDto : ConnexionDto): Observable<ResultatConnexionDto> {
    return this.httpRequestService.httpPost('Enseignant', 'PostConnexionEnseignant', connexionDto);
  }

  /**
   * Obtiens l'enseignant actuel.
   * @returns {Enseignant | null} L'enseignant actuel. (Possiblement null)
   */
  obtenirEnseignant(): Enseignant | null {
    const jwtPayload = this.jwtHelper.decodeToken() as JwtPayloadDto;
    let enseignant: Enseignant | null = null;
    if(jwtPayload){
      enseignant = {
        id: jwtPayload.sub,
        adresseCourriel: jwtPayload.email,
        prenom: jwtPayload["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname"],
        nom: jwtPayload["http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name"]
      } as Enseignant
    }

    return enseignant;
  }

  /**
   * Permet de vérifier si un enseignant est connecté ou non
   */
  estConnecte() {
    return this.jwtHelper.decodeToken() !== null && !this.jwtHelper.isTokenExpired();
  }

  /**
   * Déconnecte l'enseignant.
   * @param {enseignant} enseignant Enseignant à déconnecter.
   */
  deconnexion(enseignant: Enseignant) {
    if(enseignant) {
      localStorage.removeItem('access_token');
    }
    this.router.navigate(['/']);
  }
}
