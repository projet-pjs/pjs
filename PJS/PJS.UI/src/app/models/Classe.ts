
/**
 * Représente une classe servant à contenir une série d'exercice à réaliser
 */
export class Classe {

  public id: number  = 0;

  public nom: string  = '';

  public description: string  = '';

  public codeAcces: string = '';

  public enseignantId: number = 0;

  public estActive: boolean = false;

}
