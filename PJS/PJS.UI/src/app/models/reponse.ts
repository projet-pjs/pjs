
export class Reponse {

  public etudiantId: number = 0;

  public exerciceId: number = 0;

  public codePJS: string = '';

  public resultatConsole = '';

  public estConsulteParEtudiant: boolean = true;

  public estConsulteParEnseignant: boolean = false;

  public estReussi: boolean = false;

  public commentaire: string | null = '';
  
}
