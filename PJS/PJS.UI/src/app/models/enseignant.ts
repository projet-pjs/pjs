
/**
 * Représente le compte utilisateur d'un enseignant
 */
export class Enseignant {

  public id: number = 0

  public prenom: string  = null!;

  public nom: string = null!;

  public adresseCourriel: string = null!;


}
