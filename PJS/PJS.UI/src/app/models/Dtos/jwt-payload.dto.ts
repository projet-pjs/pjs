export interface JwtPayloadDto {
  "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/name": string;
  "http://schemas.xmlsoap.org/ws/2005/05/identity/claims/givenname": string;
  email: string;
  sub: number;


}
