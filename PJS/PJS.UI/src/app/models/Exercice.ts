
/**
 * Représente un exercice possible de réaliser
 */
export class Exercice {

  public id: number = 0;

  public nom: string = '';

  public description: string = '';

  public indexSequence: number = 0;

  public resultatAttenduCompilation: string = '';

  public demarcheAttendu: string = '';

  public dateEcheance: Date = new Date();

  public classeId?: number = 0;

}
