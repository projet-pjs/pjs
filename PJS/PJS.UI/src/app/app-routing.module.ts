import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AccueilComponent } from './components/pages/accueil/accueil.component';
import { PseudoDocComponent } from './components/pages/pseudo-doc/pseudo-doc.component';
import { LicenceComponent } from "./components/pages/licence/licence.component";
import { ConnexionEnseignantComponent } from "./components/pages/portail-enseignant/connexion-enseignant/connexion-enseignant.component";

import { PageGestionClassesComponent } from "./components/pages/portail-enseignant/page-gestion-classes/page-gestion-classes.component";
import { PageCreationClasseComponent } from "./components/pages/portail-enseignant/page-creation-classe/page-creation-classe.component";
import {
  PageModificationClasseComponent
} from "./components/pages/portail-enseignant/page-modification-classe/page-modification-classe.component";
import {
  PageCreationExerciceComponent
} from "./components/pages/portail-enseignant/page-creation-exercice/page-creation-exercice.component";
import { ListageExerciceEtudiant } from './components/pages/listage-exercice-etudiant/listage-exercice-etudiant.component';
import { PageTacheComponent } from './components/pages/page-tache/page-tache.component';

const routes: Routes = [
  { path: '', component: AccueilComponent },
  { path: 'exercice/:id', component: PageTacheComponent },
  { path: 'pseudo-doc', component: PseudoDocComponent },
  { path: 'portail-enseignant/connexion', component: ConnexionEnseignantComponent },
  { path: 'portail-enseignant/gestion', component: PageGestionClassesComponent },
  { path: 'portail-enseignant/nouvelle-classe', component: PageCreationClasseComponent },
  { path: 'portail-enseignant/modification-classe/:id', component: PageModificationClasseComponent },
  { path: 'portail-enseignant/nouvel-exercice', component: PageCreationExerciceComponent },
  { path: 'licence', component: LicenceComponent },
  { path: 'exercices/:code', component: ListageExerciceEtudiant },
  { path: '', redirectTo: '/accueil', pathMatch: 'full' },
  { path: '**', redirectTo: '/accueil' }
];

@NgModule({
  declarations: [

  ],
  imports: [
    RouterModule.forRoot(routes),
  ],
  providers: [],
  exports: [
    RouterModule
  ]
})

export class AppRoutingModule {

}
