using PJS.Application.Services;
using PJS.Application.Services.Anciens;
using PJS.Application.Services.Interfaces;
using PJS.Data.Configurations;
using PJS.Data.Context;
using PJS.Data.Repositories;
using PJS.Data.Repositories.Anciens;
using PJS.Data.Repositories.Interfaces;

var builder = WebApplication.CreateBuilder(args);
var configuration = builder.Configuration;

// Add services to the container
builder.Services.AddControllers();

// Ajoute les migrations
builder.Services.AjouterMigrations(configuration.GetConnectionString("AppDatabaseConnection")!);

builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();
builder.Services.AddSingleton<DapperContext>();


// Repertoires
builder.Services.AddSingleton<IAdminRepository, AdminRepository>();
builder.Services.AddSingleton<IClasseRepository, ClasseRepository>();
builder.Services.AddSingleton<IEnseignantRepository, EnseignantRepository>();
builder.Services.AddSingleton<IEtudiantRepository, EtudiantRepository>();
builder.Services.AddSingleton<IExerciceRepository, ExerciceRepository>();
builder.Services.AddSingleton<IReponseRepository, ReponseRepository>();


// Services 
builder.Services.AddSingleton<IAdminService, AdminService>();
builder.Services.AddSingleton<IClasseService, ClasseService>();
builder.Services.AddSingleton<IEnseignantService, EnseignantService>();
builder.Services.AddSingleton<IEtudiantService, EtudiantService>();
builder.Services.AddSingleton<IExerciceService, ExerciceService>();
builder.Services.AddSingleton<IReponseService, ReponseService>();


// Ancien
builder.Services.AddSingleton<IApplicationRepository, ApplicationRepository>();
builder.Services.AddSingleton<IApplicationService, ApplicationService>();


builder.Services.AddCors(options =>
{
    options.AddPolicy("AllowSpecificOrigin",
        builder =>
        {
            builder.WithOrigins("http://localhost:4200", "http://127.0.0.1:4200")
                   .AllowAnyHeader()
                   .AllowAnyMethod();
        });
});

Dapper.DefaultTypeMap.MatchNamesWithUnderscores = true;


var app = builder.Build();


if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseCors("AllowSpecificOrigin");

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();



/// Rend le fichier "Program" accessible aux autres projets 
namespace PJS.API
{

    /// <summary>
    /// Classe du programme
    /// </summary>
    public partial class Program
    {


    }

}
