﻿using Microsoft.AspNetCore.Mvc;
using PJS.Application.Services.Interfaces;
using PJS.Domain.Models;

namespace PJS.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ExerciceController : ControllerBase
{

    private readonly IExerciceService _exerciceService;

    public ExerciceController(IExerciceService exerciceService)
    {
        _exerciceService = exerciceService;
    }
    
    [HttpPost("Create")]
    public async Task<IActionResult> Create(Exercice exercice)
    {
        return Ok(await _exerciceService.Create(exercice));
    }

    #region GET 

    /// <summary>
    /// Retourne toutes les matières
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetAllExercicesAsync")]
    public async Task<List<Exercice>> GetAllExercices()
    {
        return await _exerciceService.GetAllExercices();
    }

    /// <summary>
    /// Retourne les exercices d'une classe
    /// </summary>
    /// <param name="classeId">Identifiant de la classe</param>
    /// <returns></returns>
    [HttpGet("FindByClasseId")]
    public async Task<IActionResult> FindByClasseId(int classeId)
    {
        return Ok(await this._exerciceService.FindByClasseId(classeId));
    }

    #endregion
    
}