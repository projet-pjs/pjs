﻿using Microsoft.AspNetCore.Mvc;
using PJS.Application.Services.Anciens;
using PJS.Domain.Models.AnciensModels;

namespace PJS.API.Controllers.Anciens;

/// <summary>
/// 
/// CE CONTRÔLLEUR SERA SUPPRIMÉ LORSQUE LA MIGRATION 
/// VERS LA NOUVELLE VERSION DE L'APPLICATION SERA TERMINÉE
/// 
/// </summary>
[Route("api/[controller]")]
[ApiController]
public class ApplicationController : ControllerBase
{
    #region Propriétés de classe


    /// <summary>
    /// Service de l'application générale
    /// </summary>
    private readonly IApplicationService _applicationService;


    #endregion

    #region Constructeur


    /// <summary>
    /// Constructeur
    /// </summary>
    public ApplicationController(IApplicationService applicationService)
    {
        _applicationService = applicationService;
    }


    #endregion

    #region GET 

    /// <summary>
    /// Retourne toutes les matières
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetAllMatieres")]
    public async Task<IActionResult> GetAllMatieres()
    {
        return Ok(await _applicationService.GetAllMatieres());
    }

    /// <summary>
    /// Retourne toutes les taches
    /// </summary>
    /// <returns></returns>
    [HttpGet("GetAllTaches")]
    public async Task<IActionResult> GetAllTaches()
    {
        return Ok(await _applicationService.GetAllTaches());
    }

    /// <summary>
    /// Retourne toutes les taches correspondant à l'identifiant d'une matière
    /// </summary>
    /// <param name="id"></param>
    /// <returns></returns>
    [HttpGet("GetTachesByMatiereId")]
    public async Task<IActionResult> GetTachesByMatiereId(int id)
    {
        return Ok(await _applicationService.GetTachesByMatiereId(id));
    }

    /// <summary>
    /// Retourne une tâche selon son identifiant
    /// </summary>
    /// <param name="id">Identifiant de la tâche</param>
    /// <returns></returns>
    [HttpGet("GetTacheById")]
    public async Task<IActionResult> GetTacheById(int id)
    {
        return Ok(await _applicationService.GetTacheById(id));
    }

    /// <summary>
    /// Retourne une matière selon son identifiant
    /// </summary>
    /// <param name="id">Identifiant de la matière</param>
    /// <returns></returns>
    [HttpGet("GetMatiereById")]
    public async Task<IActionResult> GetMatiereById(int id)
    {
        return Ok(await _applicationService.GetMatiereById(id));
    }
    

    #endregion
    
}
