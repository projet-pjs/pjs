﻿using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using PJS.Application.Dtos.Auth;
using PJS.Application.Services.Interfaces;
using PJS.Domain.Models;

namespace PJS.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class EnseignantController : ControllerBase
{

    private readonly IEnseignantService _enseignantService;
    private readonly IConfiguration _configuration; 

    public EnseignantController(IEnseignantService enseignantService,
        IConfiguration configuration)
    {
        _enseignantService = enseignantService;
        _configuration = configuration;
    }
    
    [HttpPost("PostConnexionEnseignant")]
    public async Task<IActionResult> Connexion(ConnexionDto connexionDto)
    {
        Enseignant? enseignant = await _enseignantService.GetEnseignantParCourrielMdp(connexionDto.Courriel, connexionDto.MotDePasse);

        if (enseignant == null)
            return Unauthorized();
        
        string token = CreerToken(enseignant); 
        
        return Ok(new ResultatConnexionDto() { Token = token }); 
    }

    private string CreerToken(Enseignant enseignant)
    {
        var securityKey = new SymmetricSecurityKey(Encoding.UTF8.GetBytes(_configuration["JwtSecurityKey"]!));
        
        var credentials = new SigningCredentials(securityKey, SecurityAlgorithms.HmacSha256);
            
        
        DateTime expirationDateTime = DateTime.Now.AddDays(Convert.ToInt32(_configuration["JwtExpirationJours"]!)); 
        
        List<Claim> claims =
        [
            new Claim(ClaimTypes.Name, enseignant.Nom),
            new Claim(ClaimTypes.GivenName, enseignant.Prenom),
            new Claim(JwtRegisteredClaimNames.Email, enseignant.AdresseCourriel),
            new Claim(JwtRegisteredClaimNames.Sub, enseignant.Id.ToString()),
        ];
            
        var token = new JwtSecurityToken( 
            _configuration["JwtIssuer"], 
            _configuration["JwtAudience"], 
            claims, 
            expires: expirationDateTime, 
            signingCredentials: credentials 
        ); 

        return new JwtSecurityTokenHandler().WriteToken(token); 
    }
    
    [HttpGet("CreerEnseignant")]
    public async Task<Enseignant> CreerEnseignant(Enseignant enseignant)
    {
        return await _enseignantService.CreerEnseignant(enseignant);
    }

}
