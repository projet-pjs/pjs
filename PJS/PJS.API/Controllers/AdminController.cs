﻿using Microsoft.AspNetCore.Mvc;
using PJS.Application.Services.Interfaces;

namespace PJS.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class AdminController : ControllerBase
{

    private readonly IAdminService _adminService;

    public AdminController(IAdminService adminService)
    {
        _adminService = adminService;
    }

}
