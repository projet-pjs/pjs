﻿using Microsoft.AspNetCore.Mvc;
using PJS.Application.Services.Interfaces;

namespace PJS.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ReponseController : ControllerBase
{

    private readonly IReponseService _reponseService;

    public ReponseController(IReponseService reponseService)
    {
        _reponseService = reponseService;
    }


    #region GET 

    /// <summary>
    /// Retourne les exercices d'une classe
    /// </summary>
    /// <param name="exerciceIds">Liste d'identifiants d'exercices</param>
    /// <param name="etudiantId">Identifiant de l'utilisateur actuel</param>
    /// <returns></returns>
    [HttpGet("GetReponsesPourExercices")]
    public async Task<IActionResult> GetReponsesPourExercices([FromQuery(Name = "exerciceIds")] List<int> exerciceIds, int etudiantId)
    {
        return Ok(await this._reponseService.GetReponsesPourExercices(exerciceIds, etudiantId));
    }


    #endregion


}
