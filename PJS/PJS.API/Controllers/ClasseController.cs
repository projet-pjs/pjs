﻿using Microsoft.AspNetCore.Mvc;
using PJS.Application.Services.Interfaces;
using PJS.Domain.Models;

namespace PJS.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class ClasseController : ControllerBase
{

    private readonly IClasseService _classeService;

    public ClasseController(IClasseService classeService)
    {
        _classeService = classeService;
    }

    /// <summary>
    /// Retourne une classe selon son identifiant
    /// </summary>
    /// <param name="id">Identifiant de la classe</param>
    /// <returns></returns>
    [HttpGet("FindById")]
    public async Task<IActionResult> FindById(int id)
    {
        return Ok(await _classeService.FindById(id));
    }
    
    /// <summary>
    /// Retourne la liste des classes d'un enseignant
    /// </summary>
    /// <param name="enseignantId">Identifiant d'un enseignant</param>
    /// <returns></returns>
    [HttpGet("FindByEnseignantId")]
    public async Task<IActionResult> FindByEnseignantId(int enseignantId)
    {
        return Ok(await _classeService.FindByEnseignantId(enseignantId));
    }

    /// <summary>
    /// Retourne une classe selon son code d'accès
    /// </summary>
    /// <param name="codeAcces">Code d'accès de la classe</param>
    /// <returns></returns>
    [HttpGet("FindByCodeAcces")]
    public async Task<IActionResult> FindByCodeAcces(string codeAcces)
    {
        return Ok(await _classeService.FindByCodeAcces(codeAcces));
    }
    
    /// <summary>
    /// Permet de créer une classe
    /// </summary>
    /// <param name="classe">La classe a créée</param>
    /// <returns></returns>
    [HttpPost("Create")]
    public async Task<IActionResult> Create(Classe classe)
    {
        return Ok(await _classeService.Create(classe));
    }

    /// <summary>
    /// Peremt de mettre à jour une classe
    /// </summary>
    /// <param name="classe">la classe à mettre à jour</param>
    /// <returns></returns>
    [HttpPatch("Update")]
    public async Task<IActionResult> Update(Classe classe)
    {
        return Ok(await _classeService.Update(classe));
    }
    
    
    /// <summary>
    /// Permet de supprimer une classe
    /// </summary>
    /// <param name="id">L'identifiant de la classe à supprimer</param>
    /// <returns></returns>
    [HttpDelete("Delete")]
    public async Task<IActionResult> Delete(int id)
    {
        return Ok(await _classeService.Delete(id));
    }

    /// <summary>
    /// Vérifie si la classe est encore ouverte
    /// </summary>
    /// <param name="classeId">Identifiant de la classe</param>
    /// <returns></returns>
    [HttpGet("VerifieClasseEstEncoreOuverte")]
    public async Task<IActionResult> VerifieClasseEstEncoreOuverte(int classeId)
    {
        return Ok(await _classeService.VerifieClasseEstEncoreOuverte(classeId));
    }
}
