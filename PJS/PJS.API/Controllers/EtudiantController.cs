﻿using Microsoft.AspNetCore.Mvc;
using PJS.Application.Services.Interfaces;
using PJS.Domain.Models;

namespace PJS.API.Controllers;

[Route("api/[controller]")]
[ApiController]
public class EtudiantController : ControllerBase
{

    private readonly IEtudiantService _etudiantService;

    public EtudiantController(IEtudiantService etudiantService)
    {
        _etudiantService = etudiantService;
    }

    #region GET 

    /// <summary>
    /// Retourne un étudiant selon son identifiant
    /// </summary>
    /// <param name="etudiant"></param>
    /// <returns></returns>
    [HttpGet("FindById")]
    public async Task<IActionResult> FindById(int id)
    {
        return Ok(await this._etudiantService.FindById(id));
    }

    /// <summary>
    /// Retourne un étudiant selon son identifiant
    /// </summary>
    /// <param name="id">Identifiant de l'étudiant</param>
    /// <param name="classeId">Identifiant de la classe sélectionnée</param>
    /// <returns></returns>
    [HttpGet("EstDansClasseSelectionne")]
    public async Task<IActionResult> EstDansClasseSelectionne(int id, int classeId)
    {
        return Ok(await this._etudiantService.EstDansClasseSelectionne(id, classeId));
    }

    #endregion

    #region POST

    /// <summary>
    /// Cré un utilisateur temporaire dans une classe
    /// </summary>
    /// <returns></returns>
    [HttpPost("Create")]
    public async Task<IActionResult> Create(Etudiant etudiant)
    {
        return Ok(await this._etudiantService.Create(etudiant));
    }

    #endregion

}
